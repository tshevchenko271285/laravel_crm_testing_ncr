@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      @include('manager.sidebar')

      <div class="col-lg-10 col-md-9">
        <div>
          <h1 class="text-center">{{ __('Update report info') }}</h1>
          <br>
          <br>

          <div class="">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            @if ($errors->any())
              <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

            {!! Form::model($report, ['url' => '/update-report/' . $report->id, 'class' => 'form-horizontal']) !!}

            @include ('admin.reports.form', ['formMode' => 'create', 'customers' => $customers, 'model' => $report])

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
