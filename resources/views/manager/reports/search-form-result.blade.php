@if($items)

      <table>
        <tr>
          <th></th>
          <th>{{ __('Manufacturer') }}</th>
          <th>{{ __('Serial #') }}</th>
          <th>{{ __('Unit #') }}</th>
          <th>{{ __('QD') }}</th>
          <th>{{ __('Mfr Date') }}</th>
          <th>{{ __('Retest Date') }}</th>
          <th>{{ __('DOT/UN') }}</th>
          <th>{{ __('Size') }}</th>
        </tr>
        @foreach($items as $item)

          <tr>
            <td><input id="repostItem{{ $item->id }}" type="checkbox" name="report_items[]" value="{{ $item->id }}" data-customer="{{ $item->report->customer }}"></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->manufacturer }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->serial_number }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->unit_number }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->qd }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ ($item && $item->mfr_date) ? $item->mfr_date->format('m.Y') : '---' }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ ($item && $item->ret_date) ? $item->ret_date->format('m.Y') : '---' }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->dot_un }}</label></td>
            <td><label for="repostItem{{ $item->id }}" class="m-0">{{ $item->size }}</label></td>
          </tr>

        @endforeach
      </table>

@endif
