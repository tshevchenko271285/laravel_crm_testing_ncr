@extends('layouts.app')

@section('content')
  <div class="container report-item-print">
    <div class="row justify-content-center">
      @include('manager.sidebar')

      <div class="col-lg-10">
          <h1 class="text-center">{{ __('Report details') }}</h1>
          <br>
          <br>

        <div>
          @if (session('status'))
            <div class="alert alert-success flash" role="alert">
              {{ session('status') }}
            </div>
          @endif

          <div class="row">
            <div class="col-md-4">
              <i>{{ __('Customer') }}:</i>
              <b>{{ $report->customer }}</b>
            </div>
            <div class="col-md-4">
              <i>{{ __('Bill To') }}:</i>
              <b>{{ $report->client }}</b>
            </div>
            <div class="col-md-4">
              <i>{{ __('Report ID') }}:</i>
              <b>{{ $report->number }}</b>
            </div>
            @if($report->number_type === $report::TYPE_RECEIVER)
              <div class="col-md-4">
                <i>{{ __('Rcvd Date') }}:</i>
                <b>{{ $report->rcvd_date ? $report->rcvd_date->format('m.d.Y') : '---' }}</b>
              </div>
            @else
              <div class="col-md-4">
                <i>{{ __('Service Date') }}:</i>
                <b>{{ $report->wash_date ? $report->wash_date->format('m.d.Y') : '---' }}</b>
              </div>
            @endif
            <div class="col-md-4">
              <i>{{ __('Inspector') }}:</i>
              <b>{{ $report->inspector }}</b>
            </div>
            @if($report->off_lease)
              <div class="col-md-4">
                <i>{{ __('Off Lease') }}:</i>
                <b>Yes</b>
              </div>
            @endif
            @if($report->notes)
            <div class="col-md-12">
              <i>{{ __('Notes') }}:</i> {{ $report->notes }}
            </div>
            @endif
            <br><br>
          </div>

          <div class="row d-print-none">
            <div class="col-md-12 text-right">
              @if($report->number_type === $report::TYPE_RECEIVER && $report->status == $report::STATUS_SUBMITTED)
                {!! Form::open(array('url' => '/clone-report/' . $report->id)) !!}

                  {!! Form::hidden('items', null, ['id' => 'cloneReportItems']); !!}

                  <button id="selectAllOfCloneReports" class="btn btn-outline-success" type="button">{{ __('Select All') }}</button>

                  {!!
                    Form::submit(
                      'COPY',
                      [
                        'class' => 'btn btn-primary',
                        'id' => 'cloneReportSubmit',
                        'disabled' => 'disabled',
                      ]
                    )
                  !!}

                {{ Form::close() }}
              @endif
              @if (isset($editedItem) || $errors->any())
                <button class="btn btn-outline-success btn-sm" onclick="window.print()">
                  <i class="fa fa-print" aria-hidden="true"></i>
                </button>
              @endif
              <a id="edit-report" href="/edit-report/{{ $report->id }}" class="btn btn-link">
                <i class="fa fa-pencil-alt"></i>
                {{ __('Update info') }}
              </a>
            </div>
            @if($report->number_type === $report::TYPE_SERVICE)
              <div class="col-md-12">
                <div class="search-report-items">

                  <div class="d-flex search-report-items__forms">

                    <form class="form-inline my-2 my-lg-0 mr-2 search-report-items__form">
                      <div class="input-group">
                        <input type="text" data-search-type="{{ \App\Services\ReportItemService::TYPE_SEARCH_BY_REPORT }}" class="form-control" placeholder="Search report">
                        <span class="input-group-append">
                          <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </span>
                      </div>
                    </form>

                    <form class="form-inline my-2 my-lg-0 search-report-items__form">
                      <div class="input-group">
                        <input type="text" data-search-type="{{ \App\Services\ReportItemService::TYPE_SEARCH_BY_ITEM }}" class="form-control" placeholder="Search Serial #">
                        <span class="input-group-append">
                          <button class="btn btn-secondary" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </span>
                      </div>
                    </form>

                  </div>

                  <div class="search-report-items__result">
                    @error('report_items')
                      <div class="my-3 flash alert alert-danger">{{ $message }}</div>
                    @enderror
                    <form
                      action="{{ route('add-report-items', $report) }}"
                      method="POST"
                      class="text-center search-report-items__result-form search-report-items__result-form--hidden"
                    >
                      <div class="search-report-items__result-form-content"></div>
                      @csrf
                      <div class="search-report-items__buttons">
                        <button type="button" id="selectAllFoundElements" class="btn btn-outline-success">{{ __('Select All') }}</button>
                        <input type="submit" value="{{ __('Add items') }}" class="btn btn-primary" data-customer="{{ $report->customer }}">
                      </div>
                    </form>
                    <p class="search-report-items__result-error"></p>
                  </div>
                </div>
              </div>
            @endif
          </div>
        </div>


        <div class="report-items" id="report-items" @if (isset($editedItem) || $errors->any()) style="display: none" @endif>
          @if ($report->items()->count())
            <div class="row">
              <div class="col-md-12" style="overflow-y: auto; margin-top: 1rem">
                <table class="table table-striped report-items-table">
                  <thead>
                  <tr>
                    @if($report->number_type === $report::TYPE_RECEIVER && $report->status == $report::STATUS_SUBMITTED)
                      <th>{{ __('Add to service') }}</th>
                    @endif
                    <th class="report-items__th report-items__th--number"></th>
                    <th>{{ __('Manufacturer') }}</th>
                    <th>{{ __('Serial #') }}</th>
                    <th>{{ __('Unit #') }}</th>
                    @if($report->number_type === $report::TYPE_SERVICE)
                      <th>{{ __('From report') }}</th>
                    @endif
                    <th>{{ __('QD') }}</th>
                    <th>{{ __('5YT') }}</th>
                    <th>{{ __('Mfr Date') }}</th>
                    <th>{{ __('30mo Retest Date') }}</th>
                    <th>{{ __('DOT/UN') }}</th>
                    <th>{{ __('Capacity') }}</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($report->items as $item)
                    <tr>
                      @if($report->number_type === 'receiver' && $report->status == $report::STATUS_SUBMITTED)
                        <td><input type="checkbox" class="make-clone-item" value="{{$item->id}}"></td>
                      @endif
                      <td>
                        {{ $loop->iteration }}
                        @if($item->has_certificate)
                          <span class="report-items__certificate-tooltip">
                            <i class="far fa-file-alt" data-toggle="tooltip" data-placement="bottom" title="With certification"></i>
                          </span>
                        @endif
                      </td>
                      <td>{{ $item->manufacturer }}</td>
                      <td>{{ $item->serial_number }}</td>
                      <td>{{ $item->unit_number }}</td>
                      @if($report->number_type === $report::TYPE_SERVICE)
                        <td>{{ $item->origin_report_number }}</td>
                      @endif
                      <td>{{ $item->qdLabel }}</td>
                      <td>{{ ($item && $item->get5yt()) ? $item->get5yt()->format('m.Y') : '---' }}</td>
                      <td>{{ ($item && $item->mfr_date) ? $item->mfr_date->format('m.Y') : '---' }}</td>
                      <td>{{ ($item && $item->ret_date) ? $item->ret_date->format('m.Y') : '---' }}</td>
                      <td>{{ $item->dot_un }}</td>
                      <td>{{ $item->size }}</td>
                      <td class="actions">
                        <a href="/edit-report-item/{{ $item->id }}#item-info-legend" class="btn btn-outline-primary">
                          @if($item->updated_at === null)
                            <i class="fas fa-exclamation-circle"></i>
                          @else
                            <i class="fa fa-pencil-alt"></i>
                          @endif
                        </a>
                        <a onclick="if (!confirm('Proceed to delete this item?')) return false;"
                           href="/delete-report-item/{{ $item->id }}" class="btn btn-outline-danger">
                          <i class="fa fa-trash"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
                <p class="p-2 border-top">
                  {{ __('Total') }}: <strong>{{ $report->items()->count() }}</strong> {{ __('containers') }}
                </p>
              </div>
              @if($report->number_type === \App\Models\Report::TYPE_SERVICE && isset($itemsTotal))
                <div class="col-md-12 pb-5">

                  <h4 class="my-5">Parts and service totals</h4>

                  <div class="row">

                    @foreach($itemsTotal as $key => $value)
                      <div class="col-sm-6 col-lg-4">
                        <div class="row p-2">
                          <div class="col-8 border border-right-0">{{ $key }}</div>
                          <div class="col-4 border border-left-0 text-center">{{ strlen($value) ? $value : '-' }}</div>
                        </div>
                      </div>
                    @endforeach

                  </div>
                </div>
              @endif
            </div>
            <div id="footer-panel" class="row">
              <div class="col-md-4 text-left">
                <a onclick="if (!confirm('Confirm cancelling and deleting all inserted data?')) return false;"
                   href="/delete-report/{{ $report->id }}" class="btn btn-outline-danger">
                  <i class="fa fa-trash"></i>
                  {{ __('Cancel & delete report') }}
                </a>
              </div>
              <div class="col-md-8 text-right">
                <button onclick="showItemForm()" type="button" class="btn btn-outline-success">
                  <i class="fa fa-plus"></i>
                  {{ __('Add item') }}
                </button>

                <a href="/submit/{{ $report->id }}" onclick="if (!confirm('Are you sure?')) return false;" type="button"
                   class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  {{ __('Submit & finish report') }}
                </a>
              </div>
            </div>
          @else

            <div class="row">
              <div class="col-sm-12 text-center">
                <hr>

                <i class="text-center" style="font-size: 1.5rem; color: #999">There are no items added yet</i>
                <br>
                <br>
                <button onclick="showItemForm()" type="button" class="btn btn-outline-success">
                  <i class="fa fa-plus"></i>
                  {{ __('Add item') }}
                </button>
                <br>
                <br>
                <a onclick="if (!confirm('Confirm cancelling and deleting all inserted data?')) return false;"
                   href="/delete-report/{{ $report->id }}" class="btn btn-outline-danger">
                  <i class="fa fa-trash"></i>
                  {{ __('Cancel & delete report') }}
                </a>
              </div>
            </div>

          @endif
        </div>

        <div class="container" id="item-form" @if (!isset($editedItem) && !$errors->any()) style="display: none" @endif>
          <div class="row">
            <div class="col-md-12">
              @if (isset($editedItem))
                {!! Form::model($editedItem, ['url' => '/update-report-item/' . $editedItem->id, 'class' => 'form-horizontal', 'id' => 'reportItemForm']) !!}
                @include('admin.reports.item_form', ['formMode' => 'edit', 'model' => $editedItem, 'reportType' => $report->number_type])
              @else
                {!! Form::open(['url' => '/store-report-item/' . $report->id, 'class' => 'form-horizontal', 'id' => 'reportItemForm']) !!}
                @include('admin.reports.item_form', ['formMode' => 'create', 'reportType' => $report->number_type])
              @endif
              {!! Form::close() !!}
            </div>
          </div>
        </div>
        </div>

      </div>
    </div>
  </div>

@endsection

<script>
  const showItemForm = function() {
    $('#report-items, #footer-panel').slideUp(100)
    $('#edit-report').css('opacity', 0)
    $('#item-form').slideDown(100)
    $([document.documentElement, document.body]).animate({
      scrollTop: 250
    }, 200);
  }
</script>
