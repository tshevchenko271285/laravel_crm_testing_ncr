@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      @include('manager.sidebar')

      <div class="col-lg-10">
        <div class="">
          <h1 class="text-center">{{ __('Add new Report') }}</h1>
          <br>
          <br>

          <div class="">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            @if ($errors->any())
              <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

            {!! Form::open(['url' => '/store-report', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('admin.reports.form', ['formMode' => 'create', 'customers' => $customers])

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
