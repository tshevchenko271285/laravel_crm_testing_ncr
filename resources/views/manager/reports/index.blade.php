@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row">
      @include('manager.sidebar')

      <div class="col-lg-10">
        <div class="card">
          <div class="card-header">Reports</div>
          <div class="card-body">

            {!! Form::open(['method' => 'GET', 'url' => '/reports', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search']) !!}
            <div class="input-group">
              <input type="text" class="form-control" name="search" placeholder="Search..."
                value="{{ request('search') }}">
              <span class="input-group-append">
                <button class="btn btn-secondary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
            {!! Form::close() !!}
            <br />
            <br />

            <div class="table-responsive">
              <table class="table table-striped reports-table">
                <thead>
                  <tr>
                    <th>{{ __('Number') }}</th>
                    <th>{{ __('Customer') }}</th>
                    <th>{{ __('Bill to') }}</th>
                    <th>{{ __('Rcv\'d Date') }}</th>
                    <th>{{ __('Service Date') }}</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($reports as $item)
                    <tr>
                      <td>{{ $item->number }}</td>
                      <td>{{ $item->customer }}</td>
                      <td>{{ $item->client }}</td>
                      <td>{{ $item ? $item->rcvd_date->format('m/d/Y') : '---' }}</td>
                      <td>{{ $item ? $item->wash_date->format('m/d/Y') : '---' }}</td>

                      <td class="text-right">
                        @if($item->number_type === $item::TYPE_SERVICE)
                        <div class="btn-group">
                          <button type="button" class="btn btn-outline-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-print" aria-hidden="true"></i>
                          </button>
                          <div class="dropdown-menu">
                            <a class="dropdown-item" target="_blank" href="{{ url('/print-report/' . $item->id) }}">Full Report</a>
                            <a class="dropdown-item" target="_blank" href="{{ url('/print-report/' . $item->id) . '?onlypartsservice=true' }}">Only Parts & Service</a>
                            <a class="dropdown-item" target="_blank" href="{{ url('/print-report/' . $item->id) . '?onlytotalsservice=true' }}">Only Parts & Service Totals</a>
                            <a class="dropdown-item" target="_blank" href="{{ url('/print-certificates/' . $item->id) }}">Certifications</a>
                          </div>
                        </div>
                        @else
                          <a target="_blank" href="{{ url('/print-report/' . $item->id) }}" title="Print Report"><button
                          class="btn btn-outline-success btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                        @endif
                        <a href="{{ url('/details/' . $item->id) }}" title="View Report"><button
                            class="btn btn-outline-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                        <a href="{{ url('/edit-report/' . $item->id) }}" title="Edit Report"><button
                            class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
                        <a
                          onclick="if (!confirm('Confirm delete?')) return false;"
                          href="{{ url('/delete-report/' . $item->id) }}"
                          title="Delete Report"
                        >
                          <button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              <div class="pagination-wrapper"> {!! $reports->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
