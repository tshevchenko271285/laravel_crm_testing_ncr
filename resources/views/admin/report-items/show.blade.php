@extends('layouts.backend')

@section('content')
  <div class="container">
    <div class="row">
      @include('admin.sidebar')

      <div class="col-lg-10">
        <div class="card">
          <div class="card-header">Item {{ $item->id }}</div>
          <div class="card-body">

            <a href="{{ url('/admin/reports/' . $item->report->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i
                  class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/admin/report-items/' . $item->id . '/edit') }}" title="Edit ReportItem"><button
                class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
            @if($item->trashed())
              {!! Form::open([
                'method' => 'PUT',
                'url' => ['/admin/report-items/restore', $item->id],
                'style' => 'display:inline',
              ]) !!}
                {!! Form::button('<i class="fas fa-trash-restore-alt" aria-hidden="true"></i> Restore', [
                  'type' => 'submit',
                  'class' => 'btn btn-outline-danger btn-sm',
                  'title' => 'Restore Report',
                  'onclick' => 'return confirm("Restore Report?")',
                ]) !!}
              {!! Form::close() !!}
            @else
              {!! Form::open([
                'method' => 'DELETE',
                'url' => ['admin/reportitems', $item->id],
                'style' => 'display:inline',
              ]) !!}
                          {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', [
                  'type' => 'submit',
                  'class' => 'btn btn-danger btn-sm',
                  'title' => 'Delete ReportItem',
                  'onclick' => 'return confirm("Confirm delete?")',
              ]) !!}
            {!! Form::close() !!}
            @endif
            <br />
            <br />

            <div class="table-responsive">
              <table class="table">
                <tbody>
                  <tr>
                    <th>Manufacturer</th>
                    <td>{{ $item->manufacturer }}</td>
                  </tr>
                  <tr>
                    <th>Serial #</th>
                    <td>{{ $item->serial_number }}</td>
                  </tr>
                  <tr>
                    <th>Unit #</th>
                    <td>{{ $item->unit_number }}</td>
                  </tr>
                  <tr>
                    <th>QD</th>
                    <td>{{ $item->qd }}</td>
                  </tr>
                  <tr>
                    <th>5YT</th>
                    <td>{{ $item->get5yt() }}</td>
                  </tr>
                  <tr>
                    <th>Mfr Date</th>
                    <td>{{ $item->mfr_date }}</td>
                  </tr>
                  <tr>
                    <th>30mo Retest Date</th>
                    <td>{{ $item->ret_date }}</td>
                  </tr>
                  <tr>
                    <th>DOT/UN</th>
                    <td>{{ $item->dot_un }}</td>
                  </tr>
                  <tr>
                    <th>SIZE</th>
                    <td>{{ $item->size }}</td>
                  </tr>

                  @if($item->report->number_type === \App\Models\Report::TYPE_SERVICE)
                    <tr>
                      <th>Parts and Service</th>
                      <td>
                        @foreach ($item->getCommentsFields() as $key => $title)
                          <b>{{ $title }}</b>: {{ !empty($item->comments[$key]) ? $item->comments[$key] : '___' }};
                        @endforeach
                      </td>
                    </tr>
                  @endif
                  @if($item->report->number_type === \App\Models\Report::TYPE_RECEIVER)
                    <tr>
                      <th>Notes</th>
                      <td>
                          {{ !empty($item->comments['notes']) ? $item->comments['notes'] : '' }};
                      </td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
