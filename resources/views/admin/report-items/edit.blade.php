@extends('layouts.backend')

@section('content')
    <div class="container report-item-print">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header d-print-none">Edit ReportItem #{{ $item->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/reports/' . $item->report_id) }}" title="Back" class="d-print-none"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <button class="btn btn-outline-success btn-sm d-print-none" onclick="window.print()">
                          <i class="fa fa-print" aria-hidden="true"></i>
                        </button>
                        <br class="d-print-none" />
                        <br class="d-print-none" />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($item, [
                            'method' => 'PATCH',
                            'url' => ['/admin/report-items', $item->id],
                            'class' => 'form-horizontal',
                            'id' => 'reportItemForm'
                        ]) !!}

                        @include('admin.reports.item_form', ['formMode' => 'edit', 'model' => $item, 'reportType' => $item->report->number_type])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
