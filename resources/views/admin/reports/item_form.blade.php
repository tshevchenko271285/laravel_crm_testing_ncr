@php
use Collective\Html\FormFacade as Form;
$model = $model ?? null;
$commentsFields = \App\Models\ReportItem::getCommentsFields();
$materials = \App\Services\ReportItemService::getMaterials();
@endphp
<fieldset>
  <legend id="item-info-legend">{{ __('IBC Info') }}</legend>

  <div class="row">
    <div class="col-10 form-group {{ $errors->has('manufacturer') ? 'has-error' : '' }}">
      {!! Form::label('manufacturer', 'Manufacturer', ['class' => 'control-label']) !!}
      {!! Form::text(
            'manufacturer',
            null,
            [
                'class' => 'form-control',
                'required' => 'required',
                'list' => 'manufacturers',
                'id' => 'searchReportItemManufacturer',
            ])
      !!}
      <datalist id="manufacturers">
        <option value="Metalcraft">
        <option value="Plymouth">
        <option value="Hoover">
        <option value="Clawson">
        <option value="Snyder">
        <option value="Precision">
        <option value="Titan">
        <option value="Transtore">
      </datalist>
      {!! $errors->first('manufacturer', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="form-group col-2 d-flex {{ $errors->has('bc') ? 'has-error' : '' }}">
      <div class="form-check mb-0 mt-auto ml-auto mr-0 pb-2">

        {!! Form::label('bcField', 'BC', ['class' => 'control-label']) !!}
        {!! Form::checkbox('bc', '1', $model ? $model->bc : false, ['class' => 'form-control', 'id' => 'bcField']); !!}
        {!! $errors->first('bc', '<p class="invalid-feedback">:message</p>') !!}
      </div>
    </div>

    <div class="form-group {{ $errors->has('serial_number') ? 'has-error' : '' }} @if($model) col-md-6 @else col-10 col-md-5 @endif">
      {!! Form::label('serial_number', 'Serial #', ['class' => 'control-label']) !!}
      {!! Form::text('serial_number', null, ['class' => 'form-control ' . ($errors->has('serial_number') ? 'is-invalid' : '') , 'required' => 'required', 'id' => 'searchReportItemMSerialNumber']) !!}
      {!! $errors->first('serial_number', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    @if(!$model)
      <div class="col-2 col-md-1 form-group d-flex align-items-end justify-content-center">
        <span id="searchReportItemBtn" class="btn btn-outline-success disabled" style="padding: 10px 15px;">
          <i class="fas fa-arrow-circle-down"></i>
        </span>
      </div>
    @endif

    <div class="col-md-6 form-group {{ $errors->has('unit_number') ? 'has-error' : '' }}">
      {!! Form::label('unit_number', 'Unit #', ['class' => 'control-label']) !!}
      {!! Form::text('unit_number', null, ['class' => 'form-control']) !!}
      {!! $errors->first('unit_number', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('qd') ? 'has-error' : '' }}">
      {!! Form::label('qd', 'QD', ['class' => 'control-label']) !!}
      {!! Form::select('qd', ['1' => 'Yes', '0' => '-'], null, ['class' => 'form-control']) !!}
      {!! $errors->first('qd', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('5yt') ? 'has-error' : '' }}">
      {!! Form::label('5yt', '5YT', ['class' => 'control-label']) !!}
      {!! Form::input('text', '5yt', ($model && $model->get5yt()) ? $model->get5yt()->format('m/Y') : null, ['class' => 'form-control ncr-datepicker--months', 'autocomplete' => 'off']) !!}
      {!! $errors->first('5yt', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('mfr_date') ? 'has-error' : '' }}">
      {!! Form::label('mfr_date', 'Mfr Date', ['class' => 'control-label']) !!}
      {!! Form::input('text', 'mfr_date', ($model && $model->mfr_date) ? $model->mfr_date->format('m/Y') : null, ['class' => 'form-control ncr-datepicker--months', 'autocomplete' => 'off', 'required' => 'required', 'data-date-end-date' => \Illuminate\Support\Carbon::now()->subMonth()->format('m/Y')]) !!}
      {!! $errors->first('mfr_date', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('ret_date') ? 'has-error' : '' }}">
      {!! Form::label('ret_date', '30mo Retest Date', ['class' => 'control-label']) !!}
      {!! Form::input('text', 'ret_date', ($model && $model->ret_date) ? $model->ret_date->format('m/Y') : null, ['class' => 'form-control ncr-datepicker--months', 'autocomplete' => 'off']) !!}
      {!! $errors->first('ret_date', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('dot_un') ? 'has-error' : '' }}">
      {!! Form::label('dot_un', 'DOT/UN', ['class' => 'control-label']) !!}
      {!! Form::text('dot_un', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Please select or type custom value', 'list' => 'dots']) !!}
      <datalist id="dots">
        <option value="UN31HA1">
        <option value="UN31HA2">
        <option value="UN31A">
        <option value="DOT57">
      </datalist>
      {!! $errors->first('dot_un', '<p class="invalid-feedback">:message</p>') !!}
    </div>
    <div class="col-md-6 form-group {{ $errors->has('size') ? 'has-error' : '' }}">
      {!! Form::label('size', 'Capacity', ['class' => 'control-label']) !!}
      {!! Form::number('size', null, ['placeholder' => 'Please select or type custom value', 'class' => 'form-control', 'required' => 'required', 'list' => 'sizes', 'min' => 0, 'step' => 1, 'autocomplete' => 'off']) !!}
      <datalist id="sizes">
        <option value="350">
        <option value="450">
        <option value="550">
      </datalist>
      {!! $errors->first('size', '<p class="invalid-feedback">:message</p>') !!}
    </div>

    <div class="col-md-6 form-group {{ $errors->has('materials_of_construction') ? 'has-error' : '' }}">
      {!! Form::label('materials_of_construction', 'Materials of Construction', ['class' => 'control-label']) !!}
      {!! Form::input('text', 'materials_of_construction', $model ? $model->materials_of_construction : '', ['class' => 'form-control', 'list' => 'materialsList']) !!}
      @if($materials)
        <datalist id="materialsList">
          @foreach($materials as $material)
            <option value="{{ $material->materials_of_construction }}">
          @endforeach
        </datalist>
      @endif
      {!! $errors->first('materials_of_construction', '<p class="invalid-feedback">:message</p>') !!}
    </div>

  </div>
</fieldset>


  <fieldset>
    <div class="row">
      @if($reportType === \App\Models\Report::TYPE_SERVICE)
        <legend class="col-12">{{ __('Parts and Service') }}</legend>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['wash_type'] ?? null }}</label>
          {!! Form::select('comments[wash_type]', \App\Models\ReportItem::WASH_TYPES, null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['test'] ?? null }}</label>
          {!! Form::select('comments[test]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['test_date'] ?? null }}</label>
          {!! Form::date('comments[test_date]', null, ['class' => 'form-control remind-fill', 'placeholder' => $commentsFields['test_date']]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['has_5yt'] ?? null }}</label>
          {!! Form::select('comments[has_5yt]', ['0' => 'No', '1' => 'Yes'], null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['5yt'] ?? null }}</label>
          {!! Form::date('comments[5yt]', null, ['class' => 'form-control remind-fill', 'placeholder' => $commentsFields['5yt']]) !!}
        </div>
        <div class="col-md-3 form-group {{ $errors->has('comments.thickness') ? 'has-error' : '' }}">
          <label>{{ $commentsFields['thickness'] ?? null }}</label>
          {!! Form::number('comments[thickness]', null, ['class' => 'form-control', 'placeholder' => 'Thickness', 'min' => 0, 'step' => 0.1]) !!}
          {!! $errors->first('comments.thickness', '<p class="invalid-feedback">:message</p>') !!}
        </div>

        <div class="col-md-3 form-group">
          <label>{{ __('Gasket package') }}</label>
          <button id="setGasketPackage" class="btn btn-outline-secondary w-100" type="button">
            {{ __('Gasket package') }}
          </button>
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['2opw'] ?? null }}</label>
          {!! Form::number('comments[2opw]', null, ['class' => 'form-control form-control--gasket-package', 'placeholder' => $commentsFields['2opw'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['2epdm'] ?? null }}</label>
          {!! Form::number('comments[2epdm]', null, ['class' => 'form-control form-control--gasket-package', 'placeholder' => $commentsFields['2epdm'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['3epdm'] ?? null }}</label>
          {!! Form::number('comments[3epdm]', null, ['class' => 'form-control form-control--gasket-package', 'placeholder' => $commentsFields['3epdm'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['multiseal'] ?? null }}</label>
          {!! Form::number('comments[multiseal]', null, ['class' => 'form-control form-control--gasket-package', 'placeholder' => $commentsFields['multiseal'], 'min' => 0, 'step' => 1]) !!}
        </div>

        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['opw'] ?? null }}</label>
          {!! Form::number('comments[opw]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['opw'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['elbow_nipple'] ?? null }}</label>
          {!! Form::number('comments[elbow_nipple]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['elbow_nipple'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['nipple'] ?? null }}</label>
          {!! Form::number('comments[nipple]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['nipple'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['ring'] ?? null }}</label>
          {!! Form::number('comments[ring]', null, ['class' => 'form-control', 'placeholder' => 'Ring', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['nut_bolt'] ?? null }}</label>
          {!! Form::number('comments[nut_bolt]', null, ['class' => 'form-control', 'placeholder' => 'Nut/Bolt', 'min' => 0, 'step' => 1]) !!}
        </div>

        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['ss_bung'] ?? null }}</label>
          {!! Form::number('comments[ss_bung]', null, ['class' => 'form-control', 'placeholder' => 'SS Bung', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['part_f'] ?? null }}</label>
          {!! Form::number('comments[part_f]', null, ['class' => 'form-control', 'placeholder' => 'Part F', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['cables'] ?? null }}</label>
          {!! Form::number('comments[cables]', null, ['class' => 'form-control', 'placeholder' => 'Cables', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['decals'] ?? null }}</label>
          {!! Form::number('comments[decals]', null, ['class' => 'form-control', 'placeholder' => 'Decals', 'min' => 0, 'step' => 1]) !!}
        </div>

        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['valve'] ?? null }}</label>
          {!! Form::number('comments[valve]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['valve'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['valve_lab'] ?? null }}</label>
          {!! Form::number('comments[valve_lab]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['valve_lab'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['valve_cap'] ?? null }}</label>
          {!! Form::number('comments[valve_cap]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['valve'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['retape'] ?? null }}</label>
          {!! Form::number('comments[retape]', null, ['class' => 'form-control', 'placeholder' => 'Retape', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['ex_cleaning'] ?? null }}</label>
          {!! Form::number('comments[ex_cleaning]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['ex_cleaning'], 'min' => 0, 'step' => 0.1]) !!}
        </div>

        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['welds'] ?? null }}</label>
          {!! Form::number('comments[welds]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['welds'], 'min' => 0, 'step' => 1, 'list' => 'welds_list']) !!}
          <datalist id="welds_list">
            <option value="30">
            <option value="60">
            <option value="90">
            <option value="120">
          </datalist>
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['guard'] ?? null }}</label>
          {!! Form::number('comments[guard]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['guard'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['leg'] ?? null }}</label>
          {!! Form::number('comments[leg]', null, ['class' => 'form-control', 'placeholder' => 'leg', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['outlet'] ?? null }}</label>
          {!! Form::number('comments[outlet]', null, ['class' => 'form-control', 'placeholder' => 'outlet', 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['ai_proof_test'] ?? null }}</label>
          {!! Form::number('comments[ai_proof_test]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['ai_proof_test'], 'min' => 0, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['passivation'] ?? null }}</label>
          {!! Form::number('comments[passivation]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['passivation'], 'min' => 0, 'max' => 2, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['new_lid'] ?? null }}</label>
          {!! Form::number('comments[new_lid]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['new_lid'], 'min' => 0, 'max' => 1, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['new_girard'] ?? null }}</label>
          {!! Form::number('comments[new_girard]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['new_girard'], 'min' => 0, 'max' => 1, 'step' => 1]) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['barcoded'] ?? null }}</label>
          {!! Form::select('comments[barcoded]', [__('No'), __('Yes')], null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3 form-group">
          <label>{{ $commentsFields['five_in_one_git'] ?? null }}</label>
          {!! Form::number('comments[five_in_one_git]', null, ['class' => 'form-control', 'placeholder' => $commentsFields['five_in_one_git'], 'min' => 0, 'step' => 1]) !!}
        </div>
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
          <div class="col-md-3 form-group">
            <label>{{ $commentsFields['leak_proof_test'] ?? null }}</label>
            {!! Form::select('comments[leak_proof_test]', [__('Passed'), __('Failed')], null, ['class' => 'form-control']) !!}
          </div>
          <div class="col-md-3 form-group">
            <label>{{ $commentsFields['external_inspection'] ?? null }}</label>
            {!! Form::select('comments[external_inspection]', [__('Passed'), __('Failed')], null, ['class' => 'form-control']) !!}
          </div>
          <div class="col-md-3 form-group">
            <label>{{ $commentsFields['internal_inspection'] ?? null }}</label>
            {!! Form::select('comments[internal_inspection]', [__('Passed'), __('Failed')], null, ['class' => 'form-control']) !!}
          </div>
          <div class="col-md-3 form-group">
            <label>{{ $commentsFields['min_wall_thickness_test'] ?? null }}</label>
            {!! Form::select('comments[min_wall_thickness_test]', [__('Passed'), __('Failed')], null, ['class' => 'form-control']) !!}
          </div>
        @endif
      @endif
      <div class="col-md-12 form-group">
        {!! Form::textarea('comments[notes]', null, ['class' => 'form-control', 'placeholder' => 'Additional Notes', 'rows' => 3]) !!}
      </div>

    </div>

  </fieldset>

<div class="form-group d-print-none">
  {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary btn-block']) !!}
</div>

<!-- Modal -->
<div class="modal fade" id="remindDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="remindDialogText" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button id="remindDialogConfirm" type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
