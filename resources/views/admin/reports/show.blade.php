@extends('layouts.backend')

@section('content')
  <div class="container-fluid report-item-print">
    <div class="row">
      @include('admin.sidebar')

      <div class="col-lg-10">
        <div class="card">
          <div class="card-header">Report {{ $report->number }}</div>
          <div class="card-body">

            <a href="{{ url('/admin/reports') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                  class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/admin/reports/' . $report->id . '/edit') }}" title="Edit Report"><button
                class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i>
                Edit</button></a>
            @if($report->trashed())
              {!! Form::open([
                'method' => 'PUT',
                'url' => ['/admin/reports/restore', $report->id],
                'style' => 'display:inline',
            ]) !!}
              {!! Form::button('<i class="fas fa-trash-restore-alt" aria-hidden="true"></i> Restore', [
                  'type' => 'submit',
                  'class' => 'btn btn-outline-danger btn-sm',
                  'title' => 'Restore Report',
                  'onclick' => 'return confirm("Confirm restore?")',
              ]) !!}
              {!! Form::close() !!}
            @else
            {!! Form::open([
                'method' => 'DELETE',
                'url' => ['admin/reports', $report->id],
                'style' => 'display:inline',
            ]) !!}
            {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', [
                'type' => 'submit',
                'class' => 'btn btn-danger btn-sm',
                'title' => 'Delete Report',
                'onclick' => 'return confirm("Confirm delete?")',
            ]) !!}
            {!! Form::close() !!}
            @endif
            <br />
            <br />

            <h4>General info</h4>

            <div class="row">
              <div class="col-md-4">
                <i>{{ __('Customer') }}:</i>
                <b>{{ $report->customer }}</b>
              </div>
              <div class="col-md-4">
                <i>{{ __('Bill To') }}:</i>
                <b>{{ $report->client }}</b>
              </div>
              <div class="col-md-4">
                <i>{{ __('Report ID') }}:</i>
                <b>{{ $report->number }}</b>
              </div>
              @if($report->number_type === $report::TYPE_RECEIVER)
                <div class="col-md-4">
                  <i>{{ __('Rcvd Date') }}:</i>
                  <b>{{ $report->rcvd_date ? $report->rcvd_date->format('m.d.Y') : '---' }}</b>
                </div>
              @else
                <div class="col-md-4">
                  <i>{{ __('Service Date') }}:</i>
                  <b>{{ $report->wash_date ? $report->wash_date->format('m.d.Y') : '---' }}</b>
                </div>
              @endif
              <div class="col-md-4">
                <i>{{ __('Inspector') }}:</i>
                <b>{{ $report->inspector }}</b>
              </div>
              @if($report->off_lease)
                <div class="col-md-4">
                  <i>{{ __('Off Lease') }}:</i>
                  <b>Yes</b>
                </div>
              @endif
              @if($report->notes)
                <div class="col-md-12">
                  <i>{{ __('Notes') }}:</i> {{ $report->notes }}
                </div>
              @endif
              <br><br>
            </div>
            <div class="report-items">
              <div class="row">
              @if ($report->items()->count())
                <div class="col-md-12">
                  <h4>Report items (Containers)</h4>
                </div>
                <div class="col-md-12" style="overflow-y: auto; margin-top: 1rem">
                  <table class="table table-striped report-items-table">
                    <thead>
                      <tr>
                        <th></th>
                        <th>{{ __('Manufacturer') }}</th>
                        <th>{{ __('Serial #') }}</th>
                        <th>{{ __('Unit #') }}</th>
                        <th>{{ __('From report') }}</th>
                        <th>{{ __('QD') }}</th>
                        <th>{{ __('5YT') }}</th>
                        <th>{{ __('Mfr Date') }}</th>
                        <th>{{ __('30mo Retest Date') }}</th>
                        <th>{{ __('DOT/UN') }}</th>
                        <th>{{ __('Capacity') }}</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($report->items as $item)
                        <tr class="{{ $item->trashed() ? 'reports-table__trash-row' : '' }}">
                          <td>
                            {{ $loop->iteration }}

                            @if($item->has_certificate)
                              <span class="report-items__certificate-tooltip">
                                <i class="far fa-file-alt" data-toggle="tooltip" data-placement="bottom" title="With certification"></i>
                              </span>
                            @endif
                          </td>
                          <td>{{ $item->manufacturer }}</td>
                          <td>{{ $item->serial_number }}</td>
                          <td>{{ $item->unit_number }}</td>
                          <td>{{ $item->origin_report_number }}</td>
                          <td>{{ $item->qdLabel }}</td>
                          <td>{{ $item->get5yt() ? $item->get5yt()->format('m.Y') : '---' }}</td>
                          <td>{{ $item->mfr_date ? $item->mfr_date->format('m.Y') : '---' }}</td>
                          <td>{{ $item->ret_date ? $item->ret_date->format('m.Y') : '---' }}</td>
                          <td>{{ $item->dot_un }}</td>
                          <td>{{ $item->size }}</td>
                          <td class="actions">
                            <a href="{{ url('/admin/report-items/' . $item->id) }}" title="View Report"><button
                              class="btn btn-outline-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                            <a href="{{ url('/admin/report-items/' . $item->id . '/edit') }}" title="Edit Report"><button
                              class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
                            @if(!$report->trashed())
                              @if($item->trashed())
                                  {!! Form::open([
                                      'method' => 'PUT',
                                      'url' => ['/admin/report-items/restore', $item->id],
                                      'style' => 'display:inline',
                                  ]) !!}
                                  {!! Form::button('<i class="fas fa-trash-restore-alt" aria-hidden="true"></i>', [
                                    'type' => 'submit',
                                    'class' => 'btn btn-outline-danger btn-sm',
                                    'title' => 'Restore Report',
                                    'onclick' => 'return confirm("Restore Report?")',
                                  ]) !!}
                                {!! Form::close() !!}
                              @else
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'url' => ['/admin/report-items', $item->id],
                                    'style' => 'display:inline',
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', [
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Report',
                                    'onclick' => 'return confirm("Confirm delete?")',
                                ]) !!}
                                {!! Form::close() !!}
                              @endif
                            @endif
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <p class="p-2 border-top">
                    {{ __('Total') }}: <strong>{{ $report->items()->count() }}</strong> {{ __('containers') }}
                  </p>
                </div>
                @if($report->number_type === \App\Models\Report::TYPE_SERVICE && isset($itemsTotal))
                  <div class="col-md-12 pb-5">

                    <h4 class="my-5">Parts and service totals</h4>

                    <div class="row">

                      @foreach($itemsTotal as $key => $value)
                        <div class="col-sm-6 col-lg-4">
                          <div class="row p-2">
                            <div class="col-8 border border-right-0">{{ $key }}</div>
                            <div class="col-4 border border-left-0 text-center">{{ strlen($value) ? $value : '-' }}</div>
                          </div>
                        </div>
                      @endforeach

                    </div>
                  </div>
                @endif
              @endif
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
