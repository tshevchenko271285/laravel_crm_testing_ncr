@php
    if (!isset($model)) $model = null;
@endphp

<fieldset>
    <legend>{{ __('General Info') }}</legend>
    <div class="row">
        <div class="col-md-5 form-group{{ $errors->has('customer') ? 'has-error' : ''}}">
            {!! Form::label('customer', 'Customer', ['class' => 'control-label']) !!}
            {!! Form::text('customer', null, ['class' => 'form-control', 'required' => 'required', 'list' => 'customers']) !!}
            <datalist id="customers">
                @foreach ($customers as $customer)
                    <option value="{{ $customer->title }}"></option>
                @endforeach
            </datalist>
            {!! $errors->first('customer', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-10 col-md-5 form-group{{ $errors->has('client') ? 'has-error' : ''}}">
            {!! Form::label('client', 'Bill To', ['class' => 'control-label']) !!}
            {!! Form::text('client', null, ['class' => 'form-control', 'required' => 'required', 'list' => 'clientsList']) !!}
            <datalist id="clientsList">
              @foreach ($clients as $client)
                <option value="{{ $client->title }}"></option>
              @endforeach
            </datalist>
            {!! $errors->first('client', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group col-2 d-flex {{ $errors->has('bc') ? 'has-error' : '' }}">
          <div class="form-check mb-0 mt-auto ml-auto mr-0 pb-2">

            {!! Form::label('owner', 'Owner', ['class' => 'control-label']) !!}<br>
            {!! Form::checkbox('owner', '1', $model ? $model->owner : false, ['class' => '', 'id' => 'owner']); !!}
            {!! $errors->first('owner', '<p class="invalid-feedback">:message</p>') !!}
          </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6 col-lg-4 form-group{{ $errors->has('rcvd_date') ? 'has-error' : ''}}">
            {!! Form::label('rcvd_date', 'Rcv\'d Date', ['class' => 'control-label']) !!}
            {!! Form::input('date', 'rcvd_date', $model ? $model->rcvd_date->format('Y-m-d') : null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('rcvd_date', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="col-md-6 col-lg-4 form-group{{ $errors->has('wash_date') ? 'has-error' : ''}}">
            {!! Form::label('wash_date', 'Service Date', ['class' => 'control-label']) !!}
            {!! Form::input('date', 'wash_date', $model ? $model->wash_date->format('Y-m-d') : null,['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('wash_date', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-6 col-lg-4 form-group{{ $errors->has('number_type') ? 'has-error' : ''}}">
          {!! Form::label('number_type', 'Report Type', ['class' => 'control-label']) !!}
          {!! Form::select('number_type', ['receiver' => 'Receiver', 'service' => 'Service Report'], $model->number_type ?? null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('number_type', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-6 col-lg-4 form-group{{ $errors->has('off_lease') ? 'has-error' : ''}}">
          {!! Form::label('off_lease', 'Off lease', ['class' => 'control-label']) !!}
          {!! Form::select('off_lease', [__('No'), __('Yes')], null, ['class' => 'form-control']) !!}
          {!! $errors->first('off_lease', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-md-12 col-lg-8 form-group{{ $errors->has('inspector') ? 'has-error' : ''}}">
          {!! Form::label('inspector', 'Inspector', ['class' => 'control-label']) !!}
          {!! Form::input('text', 'inspector', null,['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('inspector', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="col-12 form-group">
          {!! Form::label('notes', 'Special Notes', ['class' => 'control-label']) !!}
          {!! Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Special Notes', 'rows' => 3]) !!}
        </div>
    </div>
</fieldset>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Continue', ['class' => 'btn btn-primary btn-block']) !!}
</div>
