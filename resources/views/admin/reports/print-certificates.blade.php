<style>
  .certificate {
    max-width: 850px;
    margin-bottom: 100px;
    font-size: 14px;
    page-break-before: always;
  }
  .certificate__title {
    border: 1px solid #000000;
    text-align: center;
  }
  .certificate__section {
    padding-left: 40px;
    padding-right: 15px;
  }
  .certificate__section--border {
    border: 1px solid #000000;
    padding-top: 5px;
    padding-bottom: 5px;
  }
  .certificate h2 {
    margin-top: 0;
    margin-bottom: 5px;
    font-size: 18px;
  }
  .certificate h3 {
    display: inline-block;
    margin: 0;
  }
  .certificate br {
    line-height: 5px;
  }
  .certificate p {
    margin-top: 10px;
    margin-bottom: 10px;
  }
  .certificate__passed-failed {
    display: inline-block;
    padding: 5px;
  }
  .certificate__passed-failed--selected {
    border: 1px solid #000000;
  }
  .empty-space {
    height: 5px;
  }
  @media print {
    br {
      line-height: 0;
    }
  }
</style>
<body>

  @foreach($report->items as $reportItem)
    @continue(!$reportItem->has_certificate)
    <div class="certificate">
      <div>
        <b>NORTH EAST CONTAINER SERVICES:</b>
      </div>
      <h2 class="certificate__title">IBC / Portable Tank TEST and INSPECTION REPORT</h2>

      <table class="certificate__section">
        <tr>
          <td style="padding-right: 25px; vertical-align: top;">
            <b>Test facility:</b>
          </td>
          <td>
            North East Container Services<br>
            20 Industrial Drive<br>
            Keyport,, NJ 07735
          </td>
        </tr>
      </table>

      <div class="empty-space"></div>

      <div class="certificate__section">
        <b>IBC/Portable Tank Owner/Client:</b> {{ $report->owner ? $report->client : $report->customer }}
      </div>

      <div class="empty-space"></div>

      <div class="certificate__section certificate__section--border">
        <h2>Packaging Specification:</h2>
        <table width="100%">
          <tr>
            <td><b>IBC Manufacturer:</b> {{ $reportItem->manufacturer }}</td>
            <td><b>Capacity:</b> {{ $reportItem->size }} GAL.</td>
            <td></td>
          </tr>
          <tr>
            <td><b>Original Test Date:</b> {{ $reportItem->mfr_date ? Carbon\Carbon::parse($reportItem->mfr_date)->format('m/y') : '' }}</td>
            <td><b>Expired test date:</b> {{ $reportItem->ret_date ? Carbon\Carbon::parse($reportItem->ret_date)->format('m/y') : '' }}</td>
            <td><b>30mo Retest Date:</b> {{ !empty($reportItem->comments['test_date']) ? Carbon\Carbon::parse($reportItem->comments['test_date'])->format('m/d/Y') : '' }}</td>
          </tr>
          <tr>
            <td><b>Serial No:</b> {{ $reportItem->serial_number }}</td>
            <td><b>Unit No.:</b> {{ $reportItem->unit_number }}</td>
            <td></td>
          </tr>
          <tr>
            <td><b>Design Type:</b> {{ $reportItem->dot_un }}</td>
            <td colspan="2"><b>Materials of Construction:</b> {{ $reportItem->materials_of_construction }}</td>
          </tr>
        </table>
      </div>

        <div class="empty-space"></div>

      <div class="certificate__section">
        <h2>
          REPAIRS PERFORMED:
          @foreach(\Illuminate\Support\Collection::make($reportItem::getCommentsFields())->only(['valve', 'guard', 'leg', 'outlet']) as $key => $label)@continue(empty($reportItem->comments[$key]))@unless($loop->first), @endunless{{ $label }}@endforeach
          @if(!empty($reportItem->comments['notes']))<br>ADDITIONAL NOTES: {{ $reportItem->comments['notes'] }}@endif
        </h2>
      </div>

      <div class="certificate__section certificate__section--border">
        <h2 style="text-align: center">
          @if($reportItem->comments['test'] == 1 && $reportItem->comments['has_5yt'] != 1)
            <u>
              30 month Retest, Inspection and Periodic Testing<br>
              For UN IBCs 49 CFR 180.352 & DOT 57 Portable Tanks 49 CFR 180.605**
            </u>
          @else
            <u>
              30 month and 5 Year Retest, Inspection and Periodic Testing<br>
              For UN IBCs 49 CFR 180.352 & DOT 57 Portable Tanks 49 CFR 180.605**
            </u>
          @endif
        </h2>
        <table width="100%">
          <tr>
            <td><h3>I.</h3></td>
            <td>
              <h3>Leak Proof Test:</h3> Retest Pressure: <u>3.0 PSIG</u> Duration of Test: <u>5 Min.</u>
              <span class="certificate__passed-failed
                @if(!isset($reportItem->comments['leak_proof_test']) || $reportItem->comments['leak_proof_test'] != 1) certificate__passed-failed--selected @endif"
              >PASSED</span>
              <span class="certificate__passed-failed
                @if(isset($reportItem->comments['leak_proof_test']) && $reportItem->comments['leak_proof_test'] == 1) certificate__passed-failed--selected @endif"
              >FAILED</span>
            </td>
          </tr>
          <tr>
            <td><h3>II.</h3></td>
            <td>
              <h3>External Inspection:</h3>
              &nbsp;&nbsp;
              <span class="certificate__passed-failed
                @if(!isset($reportItem->comments['external_inspection']) || $reportItem->comments['external_inspection'] != 1) certificate__passed-failed--selected @endif"
              >PASSED</span>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span class="certificate__passed-failed
                @if(isset($reportItem->comments['external_inspection']) && $reportItem->comments['external_inspection'] == 1) certificate__passed-failed--selected @endif"
              >FAILED</span>
            </td>
          </tr>
          <td><h3>III.</h3></td>
          <td>
            <h3>Internal Inspection:</h3>
            &nbsp;&nbsp;&nbsp;
            <span class="certificate__passed-failed
             @if(!isset($reportItem->comments['internal_inspection']) || $reportItem->comments['internal_inspection'] != 1) certificate__passed-failed--selected @endif"
            >PASSED</span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="certificate__passed-failed
              @if(isset($reportItem->comments['internal_inspection']) && $reportItem->comments['internal_inspection'] == 1) certificate__passed-failed--selected @endif"
            >FAILED</span>
          </td>
          </tr>
        </table>
      </div>

      <div class="empty-space"></div>
      @if($reportItem->comments['test'] == 1 && $reportItem->comments['has_5yt'] == 0)
      @else
        <div class="certificate__section certificate__section--border">
          <h2 style="text-align: center">
            <u>
              5 Year Minimum Wall Thickness Examination<br>
              For UN IBCs 49 CFR 180.352**
            </u>
          </h2>
          <table width="100%">
            <tr>
              <td><h3>IV.</h3></td>
              <td>
                <h3>Minimum Wall Thickness Test:</h3>
                &nbsp;&nbsp;
                <span class="certificate__passed-failed
                  @if(!isset($reportItem->comments['min_wall_thickness_test']) || $reportItem->comments['min_wall_thickness_test'] != 1) certificate__passed-failed--selected @endif"
                >PASSED</span>
                &nbsp;&nbsp;
                <span class="certificate__passed-failed
                  @if(isset($reportItem->comments['internal_inspection']) && $reportItem->comments['internal_inspection'] == 1) certificate__passed-failed--selected @endif"
                >FAILED</span>
                @if($reportItem->comments['has_5yt'] == 1)
                  &nbsp;&nbsp;Results: {{ $reportItem->comments['thickness'] ? $reportItem->comments['thickness'] . ' mm.' : '-' }}
                @endif
              </td>
            </tr>
          </table>
        </div>
      @endif
      <div class="empty-space"></div>
      <img src="/images/certificate-sign2.jpg" style="max-height: 40px;">
      <div class="empty-space"></div>
      <p>** For the applicable portions of the citation. please consult'l-he Code of Federal Regulations 49. **</p>
      <p style="text-align: right; font-size: 12px;">
        <i>IBC PORTABLE TANK TEST & INSPECTION REPORT</i>
      </p>
    </div>

  @endforeach

<script>
  window.print();
</script>
</body>
