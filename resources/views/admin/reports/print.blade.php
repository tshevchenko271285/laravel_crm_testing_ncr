@php
$i = 0;
@endphp

<style>
  body {
    margin: 0;
    color: #000;
    background-color: #fff;
    font-size: 10px;
  }

  table {
    font-size: 10px;
    border-collapse: collapse;
  }

  h1 {
    page-break-before: always;
  }

  .report-title {
    margin-top: 5px;
    margin-bottom: 5px;
    font-size: 21px !important;
  }

  .report-overview {
    margin-top: 0;
    font-size: 14px;
  }

  .report-overview td:not(last-child) {
    padding-right: 15px;
  }

  .report-items-table--totals {
    font-size: 12px;
  }

  .report-items-table--totals td {
    /*width: 16.66%;*/
    padding: 5px;
    font-weight: bold;
  }

  .report-items-table__header th:last-child {
    width: 100%;
  }

  .report-items-table .report-overview {
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .report-items-table__row--grey {
    background-color: #cdcdcd;
    -webkit-print-color-adjust: exact;
  }

  .print-page-break {
    page-break-before: always;
  }

  .print-title-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .print-title-row__title {
    flex-grow: 1;
  }
  .print-title-row__logo {
    flex-shrink: 0;
    padding: 5px;
  }
  .print-title-row__notes {
    margin-top: 10px;
    margin-bottom: 0;
  }
  @media print {
    @page {
      size: landscape;
      margin-top: 50px;
    }
    .report-items-table--totals {
      width: 100%;
    }
  }
  .certificates-table th {
    font-size: 14px;
    text-align: left;
  }
  .certificates-table td, .certificates-table th {
    padding: 5px;
  }
  .certificates-table td:not(first-child), .certificates-table th:not(first-child) {
    padding-right: 25px;
  }
</style>
<body>
@if(request()->get('onlytotalsservice') !== 'true')
  <div class="row">
    <table border="1px" class="report-items-table">
      <thead>
        <tr class="print-empty-row">
          <td colspan="@if($report->number_type === \App\Models\Report::TYPE_RECEIVER) 12 @else 11 @endif">
            <div class="print-title-row">
              <div class="print-title-row__logo">
                <img src="/images/logo-print.jpg" alt="Company logo" width="100" height="100">
              </div>
              <div class="print-title-row__title">
                @if($report->number_type === \App\Models\Report::TYPE_SERVICE)
                  <h1 class="report-title">NORTH EAST CONTAINER PARTS & SERVICE REPORT</h1>
                @elseif($report->number_type === \App\Models\Report::TYPE_RECEIVER)
                  <h1 class="report-title">NORTH EAST CONTAINER RECEIVER REPORT</h1>
                @endif

                <table class="report-overview">
                  <tr>
                    @if($report->number_type === \App\Models\Report::TYPE_RECEIVER)
                      <td>
                        {{ __('Rcv\'d Date') }}:
                        <b>{{ $report ? $report->rcvd_date->format('m.d.Y') : '---' }}</b>
                      </td>
                    @endif
                    <td>
                      {{ __('Customer') }}:
                      <b>{{ $report->customer }}</b>
                    </td>
                    <td>
                      {{ __('Bill To') }}:
                      <b>{{ $report->client }}</b>
                    </td>
                    @if($report->number_type === \App\Models\Report::TYPE_SERVICE)
                      <td>
                        {{ __('Service Date') }}:
                        <b>{{ $report ? $report->wash_date->format('m.d.Y') : '---' }}</b>
                      </td>
                    @endif
                    <td>
                      {{ __('Report ID') }}:
                      <b>{{ $report->number }}</b>
                    </td>
                      @if($report->off_lease)
                        <td>
                          {{ __('Off Lease') }}:
                          <b>Yes</b>
                        </td>
                      @endif
                    <td>
                      {{ __('Inspector') }}:
                      <b>{{ $report->inspector }}</b>
                    </td>
                  </tr>
                </table>

                @if($report->notes)
                  <p class="print-title-row__notes"><strong>Notes:</strong> {{ $report->notes }}</p>
                @endif
              </div>
            </div>
          </td>
        </tr>
        <tr class="report-items-table__header">
          <th></th>
          <th>{{ __('Manufacturer') }}</th>
          <th>{{ __('Serial #') }}</th>
          <th>{{ __('Unit #') }}</th>
          <th>{{ __('QD') }}</th>
          <th>{{ __('5YT') }}</th>
          <th>{{ __('Mfr Date') }}</th>
          <th>{{ __('30mo Retest Date') }}</th>
          <th>{{ __('DOT/UN') }}</th>
          <th>{{ __('Capacity') }}</th>
          @if($report->number_type === \App\Models\Report::TYPE_RECEIVER)
            <th>{{ __('BC') }}</th>
          @endif
          @if($report->number_type === \App\Models\Report::TYPE_SERVICE)
            <th>{{ __('Comments') }}</th>
          @elseif($report->number_type === \App\Models\Report::TYPE_RECEIVER)
            <th>{{ __('Notes') }}</th>
          @endif
        </tr>
      </thead>
      <tbody>
        @foreach ($report->items as $item)
          <tr>
            @php
              $i++;
            @endphp
            <td style="padding: 5px">{{ $i }}</td>
            <td style="padding: 5px">{{ $item->manufacturer }}</td>
            <td style="padding: 5px; white-space: nowrap;">{{ $item->serial_number }}</td>
            <td style="padding: 5px">{{ $item->unit_number }}</td>
            <td style="padding: 5px">{{ $item->qdLabel }}</td>
            <td style="padding: 5px">{{ $item->get5yt() ? $item->get5yt()->format('m.Y') : '---' }}</td>
            <td style="padding: 5px">{{ $item->mfr_date ? $item->mfr_date->format('m.Y') : '---' }}</td>
            <td style="padding: 5px">{{ $item->ret_date ? $item->ret_date->format('m.Y') : '---' }}</td>
            <td style="padding: 5px">{{ $item->dot_un }}</td>
            <td style="padding: 5px">{{ $item->size }}</td>
            @if($report->number_type === \App\Models\Report::TYPE_RECEIVER)
              <td style="padding: 5px">{{ $item->bc ? '✔' : '' }}</td>
            @endif
            @if($report->number_type === \App\Models\Report::TYPE_SERVICE)
              <td style="padding: 5px;">
                @foreach ($item->getCommentsFields() as $key => $title)
                  @continue(empty($item->comments[$key]))
                  @continue(in_array($key, ['leak_proof_test', 'external_inspection', 'internal_inspection', 'min_wall_thickness_test']))

                  <b>{{ $title }}:</b>
                  @if($key === '5yt')
                    {{ Carbon\Carbon::parse($item->comments[$key])->format('m-d-Y') }};
                  @elseif(in_array($key, ['test', 'has_5yt']))
                    {{ $item->comments[$key] === '1' ? 'Yes' : 'No' }};
                  @elseif($key === 'thickness')
                    {{ $item->comments[$key] . __('mm') }};
                  @else
                    {{ $item->comments[$key] }};
                  @endif
                @endforeach
              </td>
            @elseif($report->number_type === \App\Models\Report::TYPE_RECEIVER)
              <td style="padding: 5px;">
                {{ !empty($item->comments['notes']) ? $item->comments['notes'] : '' }};
              </td>
            @endif
          </tr>
        @endforeach
      </tbody>
    </table>
    <br><br>
    <p>Signature:_________________</p>
    <br>
    <p>Date:_____________________</p>
  </div>
@endif
  @if($report->number_type === \App\Models\Report::TYPE_SERVICE && isset($itemsTotal) && request()->get('onlypartsservice') !== 'true')
    <div class="print-page-break">
      <div class="print-title-row">
        <div class="print-title-row__logo">
          <img src="/images/logo-print.jpg" alt="Company logo" width="100" height="100">
        </div>
        <div class="print-title-row__title">
          <h2 class="report-title">NORTH EAST CONTAINER PARTS & SERVICE TOTALS REPORT</h2>

          <table class="report-overview">
            <tr>
              <td>
                {{ __('Customer') }}:
                <b>{{ $report->customer }}</b>
              </td>
              <td>
                {{ __('Bill To') }}:
                <b>{{ $report->client }}</b>
              </td>
              <td>
                {{ __('Service Date') }}:
                <b>{{ $report ? $report->wash_date->format('m.d.Y') : '---' }}</b>
              </td>
              <td>
                {{ __('Report ID') }}:
                <b>{{ $report->number }}</b>
              </td>
              @if($report->off_lease)
                <td>
                  {{ __('Off Lease') }}:
                  <b>Yes</b>
                </td>
              @endif
              <td>
                {{ __('Inspector') }}:
                <b>{{ $report->inspector }}</b>
              </td>
            </tr>
          </table>
          @if($report->notes)
            <p class="print-title-row__notes"><strong>Notes:</strong> {{ $report->notes }}</p>
          @endif
        </div>
      </div>

      <table border="1px" class="report-items-table report-items-table--totals">
        <tbody>
          @foreach($itemsTotal->chunk(6) as $row)
            <tr class="report-items-table__row {{ $loop->even ? 'report-items-table__row--grey' : '' }}">
            @foreach($row as $key => $value)
              <td>
                {{ $key }}: {{ strlen($value) ? $value : '-' }}
              </td>
            @endforeach
            </tr>
          @endforeach

          @foreach($report->items as $reportItem)
            @if (!$reportItem->comments || !$reportItem->comments['notes'])
              @continue
            @endif
            <tr>
              <td style="padding: 5px;"><strong>{{ $reportItem->serial_number }}</strong></td>
              <td style="padding: 5px;" colspan="11">{{ $reportItem->comments['notes'] }}</td>
            </tr>
          @endforeach

        </tbody>
      </table>

      <br><br><br><br>

      <table class="certificates-table">
        <thead>
          <tr>
            <th></th>
            <th>Certified IBCs</th>
            <th colspan="2">Tests Performed</th>
          </tr>
        </thead>
        <tbody>

        @php
          $certIndex = 0;
        @endphp
          @foreach($report->items as $reportItem)
            @continue(!$reportItem->has_certificate)
            <tr>
              <td>{{ ++$certIndex }}</td>
              <td>{{ $reportItem->serial_number }}</td>
              <td>
                @if(!empty($reportItem->comments['test']) && $reportItem->comments['test'] == 1)
                  30 month – Leakproof, External Inspection, Internal Inspection
                @endif
              </td>
              <td>
                @if(!empty($reportItem->comments['has_5yt']) && $reportItem->comments['has_5yt'] == 1)
                  5yr – Minimum Wall Thickness
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  @endif
  <br><br><br><br>
  TRL#: ___________________     INSPECTOR: {{ $report->inspector }}
<script>
  window.print()
</script>
</body>
