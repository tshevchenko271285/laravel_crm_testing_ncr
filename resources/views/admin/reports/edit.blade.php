@extends('layouts.backend')

@section('content')
  <div class="container-fluid">
    <div class="row">
      @include('admin.sidebar')

      <div class="col-lg-10">
        <div class="card">
          <div class="card-header">Edit Report: {{ $report->number }}</div>
          <div class="card-body">
            <a href="{{ url('/admin/reports') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                  class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
              <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

            {!! Form::model($report, [
                'method' => 'PATCH',
                'url' => ['/admin/reports', $report->id],
                'class' => 'form-horizontal',
                'files' => true,
            ]) !!}

            @include ('admin.reports.form', ['formMode' => 'edit', 'model' => $report])

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
