import axios from "axios";

require('./bootstrap')
// import 'bootstrap-select'
import 'bootstrap-datepicker';

//hide all flash-messages after 5 seconds
window.setTimeout(() => $('.alert.flash').slideUp(100), 5000);

jQuery(function ($) {
  $('[data-toggle="tooltip"]').tooltip()
});

// Make clone a report
(function (){
  window.addEventListener('load', function (){

    const checkboxes = document.querySelectorAll('.make-clone-item');
    if(!checkboxes.length) return;

    const resultField = document.querySelector('#cloneReportItems');
    const submitBtn = document.querySelector('#cloneReportSubmit');
    const selectAllBtn = document.querySelector('#selectAllOfCloneReports');

    document.addEventListener('change', function (e){
      if(!e.target.closest('.make-clone-item')) return;

      const selectedItems = [];

      checkboxes.forEach(checkbox => {
        if(checkbox.checked) {
          selectedItems.push(checkbox.value);
        }
      });

      resultField.value = selectedItems.join(',')

      if(selectedItems.length) {
        submitBtn.removeAttribute('disabled');
      } else {
        submitBtn.setAttribute('disabled', 'disabled')
      }
    });

    selectAllBtn.addEventListener('click', function () {
      const selectedItems = [];
      checkboxes.forEach( checkbox => {
        checkbox.checked = true;
        selectedItems.push(checkbox.value);
      });
      resultField.value = selectedItems.join(',');
      submitBtn.removeAttribute('disabled');
    });

  });
})();

  window.addEventListener('load', function () {
    const container = document.querySelector('.search-report-items');
    if (!container) return;

    const resultForm = container.querySelector('.search-report-items__result-form');
    const resultFormContent = container.querySelector('.search-report-items__result-form-content');
    const errorContainer = container.querySelector('.search-report-items__result-error');
    const selectAllButton = container.querySelector('#selectAllFoundElements');
    const submitButton = container.querySelector('input[type="submit"]');

    document.addEventListener('submit', function (e) {

      const searchForm = e.target.closest('.search-report-items__form');
      if (!searchForm) return;
      e.preventDefault();

      const searchField = searchForm.querySelector('input[type="text"]');
      const searchType = searchField.getAttribute('data-search-type')
        ? searchField.getAttribute('data-search-type')
        : null;
      const value = searchField.value;

      resultForm.classList.add('search-report-items__result-form--hidden');
      resultFormContent.innerHTML = '';
      errorContainer.innerHTML = '';

      if (!value && !searchType) return;

      axios.get('/get-report-items/' + value + '/' + searchType)
        .then(function (response) {
          const result = response.data;

          if (result.length) {
            resultFormContent.innerHTML = result;
            resultForm.classList.remove('search-report-items__result-form--hidden');
          }

        })
        .catch(function (error) {
          console.error(error);
          errorContainer.innerHTML = 'There is no such report';
        });
    });

    resultForm.addEventListener('submit', validateCloneReportItemsForm);
    function validateCloneReportItemsForm (e) {
      const currentCustomer = submitButton.hasAttribute('data-customer')
        ? submitButton.getAttribute('data-customer')
        : null;
      let hasAnotherCustomer = false;

      if(currentCustomer) {
        e.preventDefault();
        const items = Array.from(resultFormContent.querySelectorAll('input[name="report_items[]"]:checked'));

        items.forEach(item => {
          const itemCustomer = item.getAttribute('data-customer') ? item.getAttribute('data-customer') : null;
          if(itemCustomer !== currentCustomer) {
            hasAnotherCustomer = true;
          }
        });

        if(
          hasAnotherCustomer === false
          || (hasAnotherCustomer === true && confirm('These customers don’t match, do you want to proceed?'))
        ) {
          resultForm.removeEventListener('submit', validateCloneReportItemsForm);
          resultForm.submit();
        }
      }
    }

    selectAllButton.addEventListener('click', function () {
      const items = resultFormContent.querySelectorAll('input[name="report_items[]"]');
      items.forEach(item => item.checked = true);
    });
  });

window.addEventListener('load', function (){
  const button = document.querySelector('#searchReportItemBtn');
  if(!button) return;

  const manufacturerField = document.querySelector('#searchReportItemManufacturer');
  if(!manufacturerField) return;

  const serialNumberField = document.querySelector('#searchReportItemMSerialNumber');
  if(!serialNumberField) return;

  let searchTimer;
  document.addEventListener('input', inputHandler )
  button.addEventListener('click', fillButtonHandler )

  function inputHandler (e) {
    if(e.target !== manufacturerField && e.target !== serialNumberField) return;

    clearTimeout(searchTimer);

    searchTimer = setTimeout(() => searchReportItem(), 300)
  }

  function searchReportItem () {
    if(!manufacturerField.value && !serialNumberField.value) return;

    button.classList.add('disabled');

    const data = {
      manufacturer: manufacturerField.value,
      serial_number: serialNumberField.value,
    };

    axios.post('/find-report-item', data)
      .then(function (response) {
        button.setAttribute('data-user-report', JSON.stringify(response.data))
        button.classList.remove('disabled');
      })
      .catch(function (error) {
        button.classList.add('disabled');
      });
  }

  const FILLABLE_FIELDS = ['unit_number', 'qd', '5yt', 'mfr_date', 'ret_date', 'dot_un', 'size'];

  function fillButtonHandler () {
    const data = button.hasAttribute('data-user-report')
      ? JSON.parse(button.getAttribute('data-user-report'))
      : null;

    if(!data) return;

    FILLABLE_FIELDS.forEach(fieldName => {
      const relatedField = document.querySelector('[name="' + fieldName + '"]');

      if(relatedField && data[fieldName]) {
        relatedField.value = data[fieldName];
      }
    });
  }
});

// Gasket package button handler on report detail page
window.addEventListener('load', function (){

  const gasketPackageButton = document.querySelector('#setGasketPackage');
  if(!gasketPackageButton) return;

  const container = gasketPackageButton.closest('.row');
  const fields = container.querySelectorAll('.form-control--gasket-package');

  gasketPackageButton.addEventListener('click', function () {
    fields.forEach(field => field.value = 1);
  });

});

window.addEventListener('load', function (){
  const reportItemForm = document.querySelector('#reportItemForm');
  if(!reportItemForm) return;

  const remindFields = Array.from(reportItemForm.querySelectorAll('.remind-fill'));
  if(!remindFields.length) return;

  const $modal = jQuery('#remindDialog');
  if(!$modal.length) return;

  const confirmButton = document.querySelector('#remindDialogConfirm');
  if(!confirmButton) return;

  reportItemForm.addEventListener('submit', validateReportItemForm);

  confirmButton.addEventListener('click', sendForm);

  function validateReportItemForm(e) {
    let remind = false;
    let fieldPlaceholder = '';

    e.preventDefault();

    remindFields.forEach(field => {
      if(!field.value) {
        remind = true;
        if(fieldPlaceholder.length) {
          fieldPlaceholder += ', ';
        }
        fieldPlaceholder += field.hasAttribute('placeholder')
          ? field.getAttribute('placeholder')
          : 'this';
      }

      return remind;
    });

    if(remind) {
      document.querySelector('#remindDialogText').innerHTML = 'Please confirm that the following fields should be empty: ' + fieldPlaceholder;
      $modal.modal('show');
    } else {
      sendForm();
    }
  }

  function sendForm() {
    reportItemForm.removeEventListener('submit', validateReportItemForm);
    reportItemForm.submit();
  }
});


window.addEventListener('load', function (){
  if(!document.querySelector('.month-date-field')) return;

  document.addEventListener('change', function (e) {
    const container = e.target.closest('.month-date-field');
    if(!container) return;

    const value = e.target.value;
    if(!value) return;

    const month = new Date(value).getMonth();
    if(isNaN(month)) return;

    container.querySelector('.month-date-field__label').innerHTML = (1 + month).toString();
  });
});

window.addEventListener('load', function () {
  if(!$(".ncr-datepicker--months").length) return;

  try {
    $.fn.datepicker.dates['en'].monthsShort = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    const monthPicker = $(".ncr-datepicker--months").datepicker({
      format: "mm/yyyy",
      startView: "months",
      minViewMode: "months",
      autoclose: true,
      clearBtn: true,
    });
  } catch (e) {
    console.log(e);
  }
})
