<?php

namespace App\Http\Requests\Admin;

use App\Models\ReportItem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ReportItemUpdateRequest extends FormRequest
{
    protected array $itemsSerialNumbers = [];

    protected function prepareForValidation()
    {
        $reportItem = ReportItem::find($this->report_item);
        $report = $reportItem->report;

        $this->itemsSerialNumbers = $report->items
            ->filter(fn($item) => $item->id !== $reportItem->id)
            ->pluck('serial_number')
            ->toArray();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial_number' => [
                'required',
                Rule::notIn($this->itemsSerialNumbers),
            ],
            'mfr_date' => ['required'],
            'comments.thickness' => ['sometimes', 'exclude_if:comments.has_5yt,0', 'required'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'comments.thickness' => 'thickness',
        ];
    }
}
