<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReportItemCreateRequest;
use App\Http\Requests\ReportItemUpdateRequest;
use App\Http\Resources\ReportItemResource;
use App\Models\Report;
use App\Models\ReportItem;
use App\Services\ReportItemService;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editReportItem(ReportItem $reportItem)
    {
        return view('manager.reports.details', ['report' => $reportItem->report, 'editedItem' => $reportItem]);
    }

    public function deleteReportItem(ReportItem $reportItem)
    {
        $reportId = $reportItem->report_id;
        $reportItem->delete();

        return redirect('details/' . $reportId)->with('status', __('Report item deleted'));
    }

    public function updateReportItem(ReportItem $reportItem, ReportItemUpdateRequest $request, ReportService $reportService)
    {
        if (!$request->user()->hasRole('manager')) {
            return redirect('/admin');
        }

        if ($reportService->updateItemFromRequest($reportItem, $request)) {
            return redirect('/details/' . $reportItem->report_id)->with('status', __('Report item updated'));
        }

        return view('manager.reports.details', ['report' => $reportItem->report]);
    }

    public function storeReportItem(Report $report, ReportItemCreateRequest $request, ReportService $reportService)
    {
        if (!$request->user()->hasRole('manager')) {
            return redirect('/admin');
        }

        $reportService->createItemFromRequest($request, $report);

        return redirect('/details/' . $report->id)->with('status', __('Report item added'));
    }

    public function getReportItems($value, $type, ReportItemService $reportItemService)
    {
        return $reportItemService->findReportItemsForCopy($type, $value);
    }

    public function addReportItems(Report $report, Request $request, ReportService $reportService)
    {
        $request->validate([
            'report_items' => ['required', 'array'],
        ]);
        $reportService->addReportItems($report, $request->report_items);

        return redirect()->route('report-details', $report);
    }

    public function findReportItem(Request $request, ReportItemService $reportItemService) {
        $request->validate([
            'manufacturer' => ['required', 'string', 'min:3'],
            'serial_number' => ['required', 'string', 'min:3'],
        ]);

        $reportItem = $reportItemService
            ->findReportItem($request->only(['manufacturer', 'serial_number']));

        return ReportItemResource::make($reportItem);
    }
}
