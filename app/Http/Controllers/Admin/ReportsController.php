<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\Customer;
use App\Models\Report;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, ReportService $reportService)
    {
        $reports = $reportService->search($request->get('search'));

        return view('admin.reports.index', compact('reports'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(int $id, ReportService $reportService)
    {
        $report = Report::withTrashed()->findOrFail($id);
        $itemsTotal = $reportService->totalItemsData($report);

        return view('admin.reports.show', compact('report', 'itemsTotal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(int $id, ReportService $reportService)
    {
        $report = Report::withTrashed()->findOrFail($id);
        $reportService->tryAccess($report, auth()->user());
        $customers = Customer::all();
        $clients = Client::all();

        return view('admin.reports.edit', compact('report', 'customers', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(int $id, Request $request, ReportService $reportService)
    {
        $report = Report::withTrashed()->findOrFail($id);
        $reportService->tryAccess($report, auth()->user());
        $reportService->updateFromRequest($report, $request);

        return redirect('admin/reports')->with('flash_message', 'Report updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Report::destroy($id);

        return redirect('admin/reports')->with('flash_message', 'Report deleted!');
    }

    public function print(int $id, ReportService $reportService)
    {
        $report = Report::withTrashed()->with(['reporter', 'items'])->findOrFail($id);
        $itemsTotal = collect($reportService->totalItemsData($report));

        return view('admin.reports.print', compact('report', 'itemsTotal'));
    }

    public function printCertificates(int $id, ReportService $reportService)
    {
        $report = Report::withTrashed()->with(['reporter', 'items'])->findOrFail($id);
        $itemsTotal = collect($reportService->totalItemsData($report));

        return view('admin.reports.print-certificates', compact('report', 'itemsTotal'));
    }

    public function restore(int $id)
    {
        Report::onlyTrashed()->findOrFail($id)->restore();

        return redirect('/admin/reports/' . $id)->with('flash_message', 'A report has been restored.');
    }
}
