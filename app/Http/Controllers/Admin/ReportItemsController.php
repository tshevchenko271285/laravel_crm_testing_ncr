<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ReportItemUpdateRequest;
use App\Models\ReportItem;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportItemsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reportItem = ReportItem::withTrashed()->findOrFail($id);

        return view('admin.report-items.show', ['item' => $reportItem]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit(int $id)
    {
        $item = ReportItem::withTrashed()->findOrFail($id);

        return view('admin.report-items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReportItemUpdateRequest $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ReportItemUpdateRequest $request, int $id, ReportService $reportService)
    {
        $item = ReportItem::findOrFail($id);
        $reportService->updateItemFromRequest($item, $request);

        return redirect('admin/reports/' . $item->report_id)->with('flash_message', 'Report Item updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $item = ReportItem::findOrFail($id);
        $reportId = $item->report_id;
        $item->delete();

        return redirect('admin/reports/' . $reportId)->with('flash_message', 'Report Item deleted!');
    }

    public function restore(int $id)
    {
        $item = ReportItem::onlyTrashed()->findOrFail($id);
        $item->restore();

        return redirect('/admin/reports/' . $item->report->id)->with('flash_message', 'A report item has been restored.');
    }
}
