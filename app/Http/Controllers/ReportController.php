<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Customer;
use App\Models\Report;
use App\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    protected ReportService $reportService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ReportService $reportService)
    {
        $this->middleware('auth');
        $this->reportService = $reportService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (request()->user()->hasRole('admin')) {
            return redirect('/admin');
        }

        $reports = $this->reportService->search($request->get('search'), $request->user()->id);

        return view('manager.reports.index', compact('reports'));
    }

    public function create()
    {
        $report = $this->reportService->getUnfinished(auth()->user());
        if ($report) {
            return redirect('/details/' . $report->id);
        }

        $customers = Customer::all();
        $clients = Client::all();

        return view('manager.reports.create', compact('customers', 'clients'));
    }

    /**
     * View list of report items and main report info
     *
     * @param Report $report
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function details(Report $report)
    {
        $this->reportService->tryAccess($report, auth()->user());
        $itemsTotal = $this->reportService->totalItemsData($report);

        return view('manager.reports.details', compact('report', 'itemsTotal'));
    }

    public function edit(Report $report)
    {
        $this->reportService->tryAccess($report, auth()->user());
        $customers = Customer::all();
        $clients = Client::all();

        return view('manager.reports.edit', compact('report', 'customers', 'clients'));
    }

    public function update(Report $report, Request $request)
    {
        if (!$request->user()->hasRole('manager')) {
            return redirect('/admin');
        }

        $this->reportService->tryAccess($report, auth()->user());
        $itemsTotal = $this->reportService->totalItemsData($report);

        if ($this->reportService->updateFromRequest($report, $request)) {
            return redirect('/details/' . $report->id)->with('status', __('Report info updated'));
        }

        return view('manager.reports.details', ['report' => $report, 'itemsTotal' => $itemsTotal]);
    }

    /**
     * @param Request $request
     *
     * @return Illuminate\Routing\Redirector|Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (!$request->user()->hasRole('manager')) {
            return redirect('/admin');
        }
        $report = $this->reportService->createFromRequest($request);

        return redirect('/details/' . $report->id)->with('status', __('Report created'));
    }

    public function submit(Report $report)
    {
        $this->reportService->tryAccess($report, auth()->user());
        $this->reportService->submit($report);

        return redirect('reports')->with('status', 'Report was submitted');
    }

    public function delete(Report $report)
    {
        $this->reportService->tryAccess($report, auth()->user());
        $report->delete();

        return redirect('reports')->with('status', __('Report cancelled'));
    }

    public function print(Report $report)
    {
        $report->load(['reporter', 'items'=> function ($query) {
            $query->withoutTrashed();
        }]);
        $itemsTotal = collect($this->reportService->totalItemsData($report));

        return view('admin.reports.print', compact('report', 'itemsTotal'));
    }

    public function printCertificates(Report $report)
    {
        $report->load(['reporter', 'items'=> function ($query) {
            $query->withoutTrashed();
        }]);
        $itemsTotal = collect($this->reportService->totalItemsData($report));

        return view('admin.reports.print-certificates', compact('report', 'itemsTotal'));
    }

    public function cloneReport(Request $request, Report $report)
    {
        $this->reportService->tryAccess($report, auth()->user());

        $request->validate([
            'items' => 'required',
        ]);

        $newReport = $this->reportService->cloneReport($report, explode(',', $request->items));

        return redirect('/details/' . $newReport->id)->with('status', 'Report was cloned');
    }
}
