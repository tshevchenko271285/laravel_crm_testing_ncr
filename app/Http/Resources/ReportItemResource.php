<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportItemResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'manufacturer' => $this->manufacturer,
            'serial_number' => $this->serial_number,
            'unit_number' => $this->unit_number,
            'qd' => $this->qd,
            '5yt' => $this->get5yt() ? $this->get5yt()->format('Y-m') : '',
            'mfr_date' => $this->mfr_date ? $this->mfr_date->format('Y-m') : '',
            'ret_date' => $this->ret_date ? $this->ret_date->format('Y-m') : '',
            'dot_un' => $this->dot_un,
            'size' => $this->size,
            'comments' => $this->comments,
        ];
    }
}
