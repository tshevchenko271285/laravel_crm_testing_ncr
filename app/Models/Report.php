<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class Report extends Model
{
    use LogsActivity, SoftDeletes, Notifiable;

    const STATUS_CREATING = 0;
    const STATUS_SUBMITTED = 1;

    const TYPE_SERVICE = 'service';
    const TYPE_RECEIVER = 'receiver';

    const PER_PAGE = 25;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer',
        'client',
        'owner',
        'rcvd_date',
        'wash_date',
        'number_type',
        'inspector',
        'off_lease',
        'notes',
    ];

    protected $dates = ['rcvd_date', 'wash_date'];

    public function items()
    {
        $relation = $this->hasMany(ReportItem::class);
        if (Auth::user()->hasRole('admin')) {
            $relation->withTrashed();
        }

        return $relation;
    }

    public function reporter()
    {
        return $this->belongsTo(User::class, 'reporter_id', 'id');
    }

    /**
     * @return string
     */
    public function getNumberAttribute(): string
    {
        if ($this->number_type == self::TYPE_SERVICE) {
            $prefix = 'S';
        } else {
            $prefix = 'R';
        }
        return $prefix . (10000 + $this->id);
    }

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
