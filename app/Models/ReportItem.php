<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class ReportItem extends Model
{
    use LogsActivity, SoftDeletes;

    const WASH_TYPES = ['n/a' => 'N/A', 'detergent' => 'Detergent Wash', 'solvent' => 'Solvent Wash', ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_items';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manufacturer',
        'serial_number',
        'unit_number',
        'qd',
        '5yt',
        'mfr_date',
        'ret_date',
        'dot_un',
        'size',
        'bc',
        'materials_of_construction',
        'origin_report_id',
        'updated_at',
        'comments',
    ];

    protected $casts = ['comments' => 'json'];

    protected $dates = ['mfr_date', 'ret_date', '5yt'];

    public function get5yt()
    {
        return $this->attributes['5yt'] ? Carbon::parse($this->attributes['5yt']) : null;
    }

    public function setMfrDateAttribute($value)
    {
        $this->attributes['mfr_date'] = $this->parseDate($value);
    }

    public function setRetDateAttribute($value)
    {
        $this->attributes['ret_date'] = $this->parseDate($value);
    }

    public function set5ytAttribute($value)
    {
        $this->attributes['5yt'] = $this->parseDate($value);
    }

    public function report()
    {
        $relation = $this->belongsTo(Report::class);
        if (Auth::user()->hasRole('admin')) {
            $relation->withTrashed();
        }

        return $relation;
    }

    public static function getCommentsFields()
    {
        return [
            'wash_type'  => 'Wash Type',
            'test'  => 'Test (30 mo)',
            'test_date'  => 'Date (30 mo)',
            'has_5yt'  => '5YT',
            '5yt'  => 'Date (5YT)',
            'thickness'  => 'Thickness',
            '2opw' => '2"OPW',
            '2epdm' => '2"EPDM',
            '3epdm' => '3"EPDM',
            'multiseal' => 'Multiseal',
            'opw'   => 'Polyvent',
            'elbow_nipple'  => 'Elbow',
            'nipple'  => 'Nipple',
            'ring'  => 'Ring',
            'nut_bolt'  => 'Nut/Bolt',
            'ss_bung'  => 'SS Bung',
            'part_f'  => 'Part F',
            'cables'  => 'Cables',
            'decals'  => 'Decals',
            'valve'  => 'Valve',
            'valve_lab'  => 'Valve Labor',
            'valve_cap'  => 'Valve CAP',
            'retape'  => 'Retape',
            'ex_cleaning'  => 'Ex Cleaning Labor (hours)',
            'welds'  => 'Welds (min)',
            'guard'  => 'Weld @ guard',
            'leg'  => 'Weld @ leg',
            'outlet'  => 'Weld @ outlet',
            'ai_proof_test' => 'Air-leak Proof Test',
            'passivation' => 'Passivation',
            'new_lid' => 'New lid',
            'new_girard' => 'New Girard',
            'barcoded' => 'Barcoded',
            'leak_proof_test' => 'Leak Proof Test',
            'five_in_one_git' => '5in1 Git',
            'external_inspection' => 'External Inspection',
            'internal_inspection' => 'Internal Inspection',
            'min_wall_thickness_test' => 'Min Wall Thickness Test',
            'notes' => 'Additional Notes',
        ];
    }

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function getOriginReportNumberAttribute(): string
    {
        $prefix = '';

        return $this->attributes['origin_report_id'] ? $prefix . (10000 + $this->attributes['origin_report_id']) : '';
    }

    public function getQdLabelAttribute() {
        return $this->qd == 1 ? 'Y' : 'N';
    }

    public function getHasCertificateAttribute(): bool {
        return (!empty($this->comments['test']) && $this->comments['test'] == 1)
            || (!empty($this->comments['has_5yt']) && $this->comments['has_5yt'] == 1);
    }

    protected function parseDate($value): ?string
    {
        if(empty($value)) return null;

        $result = null;
        try {
            $result = Carbon::parse($value)->format('Y-m-d');
        } catch (\Throwable $e) {}

        if(!$result) {
            try {
                $result = Carbon::createFromFormat('m/Y', $value)->format('Y-m-d');
            } catch (\Throwable $e) {}
        }

        return $result;
    }
}
