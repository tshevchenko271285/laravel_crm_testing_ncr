<?php
namespace App\Services;

use App\Models\MaterialsOfConstruction;
use App\Models\Report;
use App\Models\ReportItem;
use Illuminate\Database\Eloquent\Collection;

class ReportItemService {

    const TYPE_SEARCH_BY_REPORT = 'search-by-report';
    const TYPE_SEARCH_BY_ITEM = 'search-by-item';

    public function findReportItem(array $where)
    {
        return ReportItem::where($where)->latest()->firstOrFail();
    }

    public function findReportItemsForCopy(string $type, string $value): string
    {
        if ($type === self::TYPE_SEARCH_BY_REPORT) {
            $report_data = ReportService::parseReportNumber($value);
            $report = Report::with(['items'])->findOrFail($report_data['id']);
            $items = $report->items;
            $items->load('report');
        } elseif ($type === self::TYPE_SEARCH_BY_ITEM) {
            $items = ReportItem::with('report')->where('serial_number', $value)
                ->latest()
                ->get();
        }

        if($items->count() === 0) {
            abort(404);
        }

        return view('manager.reports.search-form-result', ['items' => $items])->render();
    }

    public static function getMaterials(): Collection
    {
        return ReportItem::whereNotNull('materials_of_construction')
            ->select('materials_of_construction')
            ->distinct()
            ->get();
    }

    public function saveMaterials(ReportItem $reportItem): ?MaterialsOfConstruction
    {
        $result = null;
        if($reportItem->materials_of_construction) {
            $result = MaterialsOfConstruction::firstOrCreate(['title' => $reportItem->materials_of_construction]);
        }

        return $result;
    }
}
