<?php

namespace App\Services;

use App\Models\Client;
use App\Models\Customer;
use App\Models\Report;
use App\Models\ReportItem;
use App\Models\User;
use App\Notifications\SubmitAndFinishReport;
use BadMethodCallException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use LogicException;

class ReportService
{
    public function createFromRequest(Request $request): ?Report
    {
        $report = new Report;
        $report->status = Report::STATUS_CREATING;
        $report->reporter_id = $request->user()->id;
        $report->fill($request->all());

        return $report->save() ? $report : null;
    }

    public function createItemFromRequest(Request $request, Report $report): ?ReportItem
    {
        $reportItem = new ReportItem;
        $reportItem->fill($request->all());
        $reportItem->comments = $request->get('comments');
        $reportItem->report_id = $report->id;

        return $reportItem->save() ? $reportItem : null;
    }

    public function updateItemFromRequest(ReportItem $reportItem, Request $request): bool
    {
        $data = $request->all();
        $data['bc'] = $request->has('bc');
        $reportItem->fill($data);
        $reportItem->comments = $request->get('comments');

        return $reportItem->save();
    }

    public function updateFromRequest(Report $report, Request $request): bool
    {
        $report->fill($request->all());
        $report->owner = $request->has('owner');

        return $report->save();
    }

    public function saveCustomer(Report $report): Customer
    {
        //check if customer already exist
        $customerIdentifier = $report->customer;
        $customer = Customer::where('title', $customerIdentifier)
            ->first();

        //add it to DB if not exist
        if (!$customer) {
            $customer = new Customer;
            $customer->title = $customerIdentifier;
            $customer->save();
        }

        return $customer;
    }

    public function saveClient(Report $report): Client
    {
        //check if client already exist
        $clientIdentifier = $report->client;
        $client = Client::where('title', $clientIdentifier)->first();

        //add it to DB if not exist
        if (!$client) {
            $client = new Client;
            $client->title = $clientIdentifier;
            $client->save();
        }

        return $client;
    }

    public function getUnfinished(User $user): ?Report
    {
        return Report::where('reporter_id', $user->id)
            ->where('status', Report::STATUS_CREATING)
            ->latest()
            ->first();
    }

    public function submit(Report $report): Report
    {
        if (!$report->status === Report::STATUS_CREATING) {
            throw new BadMethodCallException('Cannot submit Report that is already submitted');
        }

        $report->status = Report::STATUS_SUBMITTED;
        $report->save();

        if(env('ADMIN_EMAIL')) {
            Notification::route('mail', env('ADMIN_EMAIL'))
                ->notify(new SubmitAndFinishReport($report));
        }

        return $report;
    }

    public function tryAccess(Report $report, User $user): void
    {
        if ((!$user->hasRole('admin') && $user->hasRole('manager') && ($report->reporter_id !== $user->id))
            || ($user->hasRole('admin') && $report->status === Report::STATUS_CREATING)
        ) {
            throw new LogicException('User have no access to Report');
        }
    }

    public function totalItemsData(Report $report): array
    {
        $total = [];

        if($report->number_type === \App\Models\Report::TYPE_SERVICE) {
            $commentsFields = collect(ReportItem::getCommentsFields())
                ->except([
                    'thickness',
                    'notes', 'wash',
                    'leak_proof_test',
                    'external_inspection',
                    'internal_inspection',
                    'min_wall_thickness_test'
                ])
                ->toArray();

            $washTypes = collect(ReportItem::WASH_TYPES)->except(['n/a'])->toArray();

            $total = collect($washTypes)->merge($commentsFields)->except('wash_type')->toArray();
            $total = array_fill_keys($total, 0);

            foreach ($commentsFields as $key => $field) {
                foreach ($report->items as $reportItem) {
                    $value = 0;
                    $field_name = $field;

                    if ($key === 'wash_type') {
                        if(
                            empty($reportItem->comments[$key])
                            || $reportItem->comments[$key] == 'n/a'
                            || !isset(ReportItem::WASH_TYPES[$reportItem->comments[$key]])
                        ) {
                            continue;
                        }
                        $field_name = ReportItem::WASH_TYPES[$reportItem->comments[$key]];
                        $value = 1;
                    } elseif (isset($reportItem->comments[$key])
                        && ( $key === '5yt' || $key === 'test_date' ) ) {
                        $value = 1;
                    } else {
                        if(!empty($reportItem->comments[$key])) {
                            $value = (float) $reportItem->comments[$key];
                        }
                    }

                    $preValue = !empty($total[$field_name]) ? $total[$field_name] : 0;
                    $total[$field_name] = $preValue + $value;
                }

            }

        }

        return $total;
    }

    public function cloneReport(Report $report, array $reportItemsId): Report
    {
        $newReport = $report->replicate(['id'])->fill(['number_type' => $report::TYPE_SERVICE]);

        $reportItems = [];
        foreach (ReportItem::find($reportItemsId) as $item) {
            $reportItems[] = $item->replicate(['id'])
                ->fill([
                    'origin_report_id' => $item->report_id,
                    'updated_at' => null,
                ])->toArray();
        }

        $newReport->save();
        $newReport->items()->createMany($reportItems);

        return $newReport;
    }

    public function search(?string $keyword, ?int $reporterId = null)
    {
        if(Auth::user()->hasRole('admin')) {
            $query = Report::withTrashed();
        } else {
            $query = Report::withoutTrashed();
        }

        $query->with(['reporter'])->where('status', Report::STATUS_SUBMITTED);

        if($reporterId) {
            $query->whereHas('reporter', function ($_query) use ($reporterId) {
                $_query->where('id', $reporterId);
            });
        }
        if (!empty($keyword)) {

            $numberData = ReportService::parseReportNumber($keyword);

            $dateValidator = Validator::make(['string' => $keyword], ['string' => 'date']);
            $date = null;
            if(!$dateValidator->fails()) {
                try {
                    $date = Carbon::parse($keyword);
                } catch (\Throwable $e) {}
            }

            if ($numberData['id'] && $numberData['type']) {
                $query->where('id', $numberData['id'])->where('number_type', $numberData['type']);
            } elseif ($date) {
                $query->where(function($q) use($keyword, $date) {
                    $q->whereMonth('rcvd_date', $date->format('m'))
                        ->whereDay('rcvd_date', $date->format('d'))
                        ->whereYear('rcvd_date', $date->format('Y'));
                })->orWhere(function($q) use($keyword, $date) {
                    $q->whereMonth('wash_date', $date->format('m'))
                        ->whereDay('wash_date', $date->format('d'))
                        ->whereYear('wash_date', $date->format('Y'));
                });
            } else {
                $query->where('customer', 'LIKE', "%$keyword%") //+
                    ->orWhere('client', 'LIKE', "%$keyword%") //+
                    ->orWhereHas('items', function ($subQuery) use ($keyword) {
                        $subQuery->where('serial_number', 'LIKE', "%$keyword%") // +
                    ;
                });
            }
        }

        return $query->latest()->paginate(Report::PER_PAGE);
    }

    public function addReportItems(Report $report, array $items): void
    {
        $reportItems = [];
        foreach (ReportItem::find($items) as $item) {
            $newItem = $item->replicate(['id'])->toArray();
            $newItem['origin_report_id'] = $item->report_id;
            $newItem['updated_at'] = null;
            $reportItems[] = $newItem;
        }

        $report->items()->createMany($reportItems);
    }

    public static function parseReportNumber(string $number): array
    {
        $reportId = null;
        $reportType = null;
        if(preg_match('/^[SR]\d{5}$/', $number)) { // R10025 S65424 10025
            $reportId = (int) str_replace(['S', 'R'], '', $number, ) - 10000;

            $reportType = substr($number, 0, 1);
            switch ($reportType) {
                case 'R':  $reportType = Report::TYPE_RECEIVER; break;
                case 'S':  $reportType = Report::TYPE_SERVICE; break;
                default: $reportType = null;
            }
        } elseif(is_numeric($number)) {
            $reportId = $number - 10000;
        }

        return [
            'id' => $reportId,
            'type' => $reportType,
        ];
    }
}
