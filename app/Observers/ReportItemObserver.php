<?php

namespace App\Observers;

use App\Models\ReportItem;
use App\Services\ReportItemService;

class ReportItemObserver
{
    /**
     * Handle the ReportItem "created" event.
     *
     * @param  \App\Models\ReportItem  $reportItem
     * @return void
     */
    public function created(ReportItem $reportItem)
    {
        $this->saveMaterialsOfConstruction($reportItem);
    }

    /**
     * Handle the ReportItem "updated" event.
     *
     * @param  \App\Models\ReportItem  $reportItem
     * @return void
     */
    public function updated(ReportItem $reportItem)
    {
        $this->saveMaterialsOfConstruction($reportItem);
    }

    /**
     * Handle the ReportItem "deleted" event.
     *
     * @param  \App\Models\ReportItem  $reportItem
     * @return void
     */
    public function deleted(ReportItem $reportItem)
    {
        //
    }

    /**
     * Handle the ReportItem "restored" event.
     *
     * @param  \App\Models\ReportItem  $reportItem
     * @return void
     */
    public function restored(ReportItem $reportItem)
    {
        //
    }

    /**
     * Handle the ReportItem "force deleted" event.
     *
     * @param  \App\Models\ReportItem  $reportItem
     * @return void
     */
    public function forceDeleted(ReportItem $reportItem)
    {
        //
    }

    private function saveMaterialsOfConstruction(ReportItem $reportItem)
    {
        $reportItemService = new ReportItemService();
        $materialsOfConstruction = $reportItemService->saveMaterials($reportItem);

        ReportItem::withoutEvents(function() use ($reportItem, $materialsOfConstruction) {
            $reportItem->materials_of_construction_id = $materialsOfConstruction ? $materialsOfConstruction->id : null;
            $reportItem->save();
        });
    }
}
