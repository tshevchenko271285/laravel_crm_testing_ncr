<?php

namespace App\Observers;

use App\Models\Report;
use App\Services\ReportService;

class ReportObserver
{
    /**
     * Handle the Report "created" event.
     *
     * @param  \App\Models\Report  $report
     * @return void
     */
    public function created(Report $report)
    {
        $this->saveCustomer($report);
        $this->saveClient($report);
    }

    /**
     * Handle the Report "created" event.
     *
     * @param  \App\Models\Report  $report
     * @return void
     */
    public function updated(Report $report)
    {
        $this->saveCustomer($report);
        $this->saveClient($report);
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\Report  $report
     * @return void
     */
    public function deleted(Report $report)
    {
        $report->items()->delete();
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\Report  $report
     * @return void
     */
    public function restored(Report $report)
    {
        foreach ($report->items as $item) {
            $item->restore();
        }
    }

    private function saveCustomer(Report $report)
    {
        $reportService = new ReportService;
        $customer = $reportService->saveCustomer($report);

        Report::withoutEvents(function() use ($report, $customer) {
            $report->customer_id = $customer->id;
            $report->save();
        });
    }

    private function saveClient(Report $report)
    {
        $reportService = new ReportService;
        $client = $reportService->saveClient($report);

        Report::withoutEvents(function() use ($report, $client) {
            $report->client_id = $client->id;
            $report->save();
        });
    }
}
