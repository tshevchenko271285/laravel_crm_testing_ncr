<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('customer')->notNull();
            $table->string('client')->notNull();
            $table->timestamp('rcvd_date')->nullable();
            $table->timestamp('wash_date')->nullable();
            $table->unsignedBigInteger('reporter_id')->nullable();
            $table->unsignedInteger('customer_id')->nullable();
            $table->tinyInteger('status')->notNull()->default(0);
        });

        Schema::table('reports', function (Blueprint $table) {
            $table->foreign('reporter_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
