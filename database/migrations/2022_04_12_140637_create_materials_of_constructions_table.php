<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsOfConstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials_of_constructions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
        });

        Schema::table('report_items', function (Blueprint $table) {
            $table->foreign('materials_of_construction_id')
                ->references('id')
                ->on('materials_of_constructions')
                ->cascadeOnUpdate()
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_items', function ($table) {
            $table->dropForeign(['materials_of_construction_id_foreign']);
        });
        Schema::dropIfExists('materials_of_constructions');
    }
}
