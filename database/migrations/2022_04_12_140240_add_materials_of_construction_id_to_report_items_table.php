<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaterialsOfConstructionIdToReportItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_items', function (Blueprint $table) {
            $table->unsignedInteger('materials_of_construction_id')
                ->after('materials_of_construction')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_items', function (Blueprint $table) {
            $table->dropColumn('materials_of_construction_id');
        });
    }
}
