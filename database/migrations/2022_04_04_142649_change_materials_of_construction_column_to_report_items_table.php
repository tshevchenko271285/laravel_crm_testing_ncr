<?php

use App\Models\ReportItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMaterialsOfConstructionColumnToReportItemsTable extends Migration
{
    const MATERIALS_OF_CONSTRUCTION = ['0' => '', 1 => 'Stainless Steel', 2 => 'Carbon Steel', 3 => 'Plastic Composite'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_items', function (Blueprint $table) {
            $table->string('materials_of_construction')->change();
        });

        $items = ReportItem::whereNotNull('materials_of_construction')->get();
        foreach ($items as $item) {
            if(isset(self::MATERIALS_OF_CONSTRUCTION[$item->materials_of_construction])) {
                $item->materials_of_construction = self::MATERIALS_OF_CONSTRUCTION[$item->materials_of_construction];
                $item->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $items = ReportItem::whereNotNull('materials_of_construction')->get();
        foreach ($items as $item) {
            if(in_array($item->materials_of_construction, self::MATERIALS_OF_CONSTRUCTION)) {
                $item->materials_of_construction = array_search($item->materials_of_construction, self::MATERIALS_OF_CONSTRUCTION);
                $item->save();
            }
        }

        Schema::table('report_items', function (Blueprint $table) {
            $table->smallInteger('materials_of_construction')->change();
        });
    }
}
