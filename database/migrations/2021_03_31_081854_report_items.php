<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReportItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('manufacturer')->notNull();
            $table->string('serial_number')->notNull();
            $table->string('unit_number')->nullable();
            $table->string('qd')->nullable();
            $table->string('5yt')->nullable();
            $table->timestamp('mfr_date')->nullable();
            $table->timestamp('ret_date')->nullable();
            $table->string('dot_un')->nullable();
            $table->string('size')->nullable();
            $table->json('comments')->nullable();
            $table->unsignedInteger('report_id')->notNull();
        });

        Schema::table('report_items', function (Blueprint $table) {
            $table->foreign('report_id')
                ->references('id')
                ->on('reports')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_items');
    }
}
