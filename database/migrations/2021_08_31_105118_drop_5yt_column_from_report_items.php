<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Drop5ytColumnFromReportItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('report_items', function (Blueprint $table) {
            $table->dropColumn('5yt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('report_items', function (Blueprint $table) {
            $table->string('5yt')->after('qd')->nullable();
        });
    }
}
