<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/reports');
});

Route::middleware(['auth'])->namespace('App\Http\Controllers')->group(function () {
    //homepage (form for managers)
    //report
    Route::get('reports', 'ReportController@index');
    Route::post('store-report', 'ReportController@store');
    Route::get('details/{report}', 'ReportController@details')->name('report-details');
    Route::get('create-report/', 'ReportController@create');
    Route::get('edit-report/{report}', 'ReportController@edit');
    Route::post('update-report/{report}', 'ReportController@update');
    Route::get('delete-report/{report}', 'ReportController@delete');
    Route::get('submit/{report}', 'ReportController@submit');
    Route::get('print-report/{report}', 'ReportController@print');
    Route::get('print-certificates/{report}', 'ReportController@printCertificates');
    Route::post('clone-report/{report}', 'ReportController@cloneReport');

    //report-item
    Route::post('store-report-item/{report}', 'ReportItemController@storeReportItem');
    Route::get('edit-report-item/{reportItem}', 'ReportItemController@editReportItem');
    Route::post('update-report-item/{reportItem}', 'ReportItemController@updateReportItem');
    Route::get('delete-report-item/{reportItem}', 'ReportItemController@deleteReportItem');
    Route::get('get-report-items/{value}/{type}/', 'ReportItemController@getReportItems');
    Route::post('add-report-items/{report}', 'ReportItemController@addReportItems')->name('add-report-items');
    Route::post('find-report-item', 'ReportItemController@findReportItem')->name('find-report-item');
});

Route::middleware(['auth'])->namespace('App\Http\Controllers\Admin')->group(function () {
    //admin side
    Route::get('admin', 'ReportsController@index');
    Route::resource('admin/roles', 'RolesController');
    Route::resource('admin/permissions', 'PermissionsController');
    Route::resource('admin/users', 'UsersController');
    Route::resource('admin/activitylogs', 'ActivityLogsController');
    Route::resource('admin/reports', 'ReportsController');
    Route::put('admin/reports/restore/{report}', 'ReportsController@restore');
    Route::get('admin/reports/{report}/print', 'ReportsController@print');
    Route::get('admin/reports/print-certificates/{report}', 'ReportsController@printCertificates');
    Route::resource('admin/report-items', 'ReportItemsController');
    Route::put('admin/report-items/restore/{report}', 'ReportItemsController@restore');
    Route::resource('admin/customers', 'CustomersController');

    //developer tools
    Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});

Auth::routes();
